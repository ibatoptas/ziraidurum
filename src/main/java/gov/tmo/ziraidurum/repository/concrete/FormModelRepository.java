package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.FormModel;
import gov.tmo.ziraidurum.service.abstacts.FormModelService;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormModelRepository extends JpaRepository<FormModel,Long> {

}
