package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.FormDataInput;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Calendar;
import java.util.List;

public interface FormDataInputRepository extends JpaRepository<FormDataInput,Long> {
    boolean existsBySube_IdAndZiraiDonem_Id(Long subeId, Long ziraiDonemId);

    <T> List<T> findProjectedByStatusTrueOrderById(Class<T> clz);

    <T> T findProjectedById(Class<T> clz, Long id);

    <T> T findBySube_IdAndZiraiDonem_Id(Class<T> clz, Long subeId, Long ziraiDonemId);
}
