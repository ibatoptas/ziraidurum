package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.EkimDonemi;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EkimDonemiRepository extends JpaRepository<EkimDonemi,Long> {

    <T> List<T> findProjectedByStatusTrueOrderById(Class<T> clz);

    <T> T findProjectedById(Class<T> clz, Long id);


}
