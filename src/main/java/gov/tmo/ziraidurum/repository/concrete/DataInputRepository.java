package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.DataInput;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DataInputRepository extends JpaRepository<DataInput,Long> {
}
