package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.Bolge;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BolgeRepository extends JpaRepository<Bolge,Long> {

    <T> List<T> findProjectedBy(Class<T> clz);

    <T> T findProjectedById(Class<T> clz, Long id);
}
