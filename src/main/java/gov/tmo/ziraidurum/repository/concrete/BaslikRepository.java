package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.Baslik;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BaslikRepository extends JpaRepository<Baslik,Long> {

    <T> List<T> findProjectedBy(Class<T> clz);
    <T> List<T> findProjectedByUruns_Id(Class<T> clz,Long urunId);
}
