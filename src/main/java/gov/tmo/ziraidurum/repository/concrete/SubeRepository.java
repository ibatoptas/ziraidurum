package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.Sube;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubeRepository extends JpaRepository<Sube,Long> {

    <T> List<T> findProjectedBy(Class<T> clz);

    <T> T findProjectedById(Class<T> clz, Long id);

    <T> T findById(Class<T> clz, Long id);

}
