package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.ZiraiDonem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ZiraiDonemRepository extends JpaRepository<ZiraiDonem,Long> {

    <T> List<T> findProjectedByStatusTrueOrderByEkimDonemiId(Class<T> clz);

    <T> T findProjectedById(Class<T> clz, Long id);

    <T> List<T> findByEkimDonemi_IdAndUrun_IdOrderByBitisTarihiDesc(Class<T> clz,Long ekimDonemId, Long urunId);

    <T> List<T> findByEkimDonemi_IdAndUrun_IdAndBaslangicTarihiLessThanEqualOrderByBitisTarihiDesc(Class<T> clz,Long ekimDonemId, Long urunId, Date today);
}
