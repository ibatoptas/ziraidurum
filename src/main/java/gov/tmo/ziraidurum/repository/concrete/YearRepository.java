package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.Year;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface YearRepository extends JpaRepository<Year,Long> {

    @Query("select y from Year y where y.yearName = ?1")
    Year findByYearName(String yearName);

    @Query("select y from Year y where y.yearName = ?1 and y.id <> ?2")
    Year findByYearNameAndIdIsNot(String yearName, Long id);

    <T> List<T> findProjectedByStatusTrueOrderByYearName(Class<T> clz);

    <T> T findProjectedById(Class<T> clz, Long id);

}
