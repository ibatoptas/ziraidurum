package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.Caption;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CaptionRepository extends JpaRepository<Caption,Long> {

    <T> List<T> findProjectedBy(Class<T> clz);
}
