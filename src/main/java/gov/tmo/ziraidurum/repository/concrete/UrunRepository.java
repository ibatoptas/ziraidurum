package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.Urun;
import gov.tmo.ziraidurum.entities.concrete.Year;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UrunRepository extends JpaRepository<Urun,Long> {

    @Query("select u from Urun u where u.urunKodu = ?1")
    Urun findByUrunKodu(String urunKodu);

    @Query("select u from Urun u where u.urunKodu = ?1 and u.id <> ?2")
    Urun findByUrunKoduAndIdNot(String urunKodu, Long id);

    <T> List<T> findProjectedByStatusTrueOrderByUrunKodu(Class<T> clz);

    <T> T findProjectedById(Class<T> clz, Long id);

    <T> List<T> findProjectedBy(Class<T> clz);

}
