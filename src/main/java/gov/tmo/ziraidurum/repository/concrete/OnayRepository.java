package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.Onay;
import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OnayRepository extends JpaRepository<Onay, Long> {


    <T> List<T> findProjectedByStatusTrue(Class<T> clz);

    <T> T findProjectedByIdAndStatusTrue(Class<T> clz, Long id);

    Optional<Onay> findByUser_IdAndFormDataInput_Id(Long userId, Long formDataInputId);

    long deleteByOnayActionAndFormDataInput_Id(EFormOnayStatus onayAction, Long id);


}
