package gov.tmo.ziraidurum.repository.concrete;

import gov.tmo.ziraidurum.entities.concrete.Groups;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupsRepository extends JpaRepository<Groups,Long> {
}
