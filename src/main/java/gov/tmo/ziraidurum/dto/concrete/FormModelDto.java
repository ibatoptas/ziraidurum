package gov.tmo.ziraidurum.dto.concrete;

import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.enums.EDataType;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Form Model Data Transfer Object")
public class FormModelDto implements IDto {

    private Byte satir;
    private Byte sutun;
    private EDataType dataType;
    private List<GroupsDTO> groups;

}
