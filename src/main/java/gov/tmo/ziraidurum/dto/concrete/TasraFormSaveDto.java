package gov.tmo.ziraidurum.dto.concrete;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Map;

/**
 * Created by iBa at 16.10.2021 11:56
 */
@Getter
@Setter
public class TasraFormSaveDto {
    @Min(0)
    private long formDataInputId;
    @NotNull
    @Positive
    private Long ziraiDonemId;
    @NotEmpty
    private Map<String,String> dataInputs;
}
