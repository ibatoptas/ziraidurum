package gov.tmo.ziraidurum.dto.concrete;

import gov.tmo.ziraidurum.entities.concrete.enums.EZiraiDonemDurum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Created By ibrahim.toptas
 * Created At 14.10.2021 - 14:32
 **/
@Getter
@Setter
@NoArgsConstructor
public class ZiraiDonemSummaryDto {
    private Long id;
    private String donemAdi;
    private Date baslangicTarihi;
    private Date bitisTarihi;
    private String aciklama;
    private Integer durumInt;
    private String durum;
    private Long girisYapmayan;
    private Long onaylanmamis;

    public ZiraiDonemSummaryDto(Long id,
                                String donemAdi,
                                Date baslangicTarihi,
                                Date bitisTarihi,
                                String aciklama,
                                Integer durum,
                                Long girisYapmayan,
                                Long onaylanmamis) {
        this.id = id;
        this.donemAdi = donemAdi;
        this.baslangicTarihi = baslangicTarihi;
        this.bitisTarihi = bitisTarihi;
        this.aciklama = aciklama;
        this.durumInt = durum;
        this.durum = EZiraiDonemDurum.values()[durum].getText();
        this.girisYapmayan = girisYapmayan;
        this.onaylanmamis = onaylanmamis;
    }


}
