package gov.tmo.ziraidurum.dto.concrete;

import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.FormModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Group Data Transfer Object")
public class GroupDto implements IDto {

    @ApiModelProperty(required = true,value = "Group ID")
    private Long id;

    @ApiModelProperty(required = true,value = "Ad")
    @NotNull
    private String ad;

    @ApiModelProperty(required = true,value = "Index")
    @NotNull
    private Byte index;

    @ApiModelProperty(required = true,value = "Form Model")
    private FormModelDto formModelDto;

    @ApiModelProperty(required = true,value = "Form Model Id")
    private Long formModelId;

//    @ApiModelProperty(required = true,value = "Group Date")
//    private Date createdAt;
//
//    @ApiModelProperty(required = true,value = "Group Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "Group Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "Group Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "Group Status")
    private Boolean status;
}
