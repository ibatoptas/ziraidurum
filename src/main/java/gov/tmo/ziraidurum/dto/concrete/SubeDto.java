package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.Bolge;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Sube Data Transfer Object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubeDto implements IDto {

    @ApiModelProperty(required = true,value = "Sube ID")
    private Long id;

    @ApiModelProperty(required = true,value = "Şube Kodu")
    @NotNull
    private String subeKodu;

    @ApiModelProperty(required = true,value = "Şube Adı")
    @NotNull
    private String subeAdi;

    @ApiModelProperty(required = true,value = "Şube bölgesi")
    private BolgeDto bolgeDto;

    @NotNull
    @ApiModelProperty(required = true,value = "Bölge ID")
    private Long bolgeId;

//    @ApiModelProperty(required = true,value = "Şube Created Date")
//    private Date createdAt;
//
//    @ApiModelProperty(required = true,value = "Şube Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "Şube Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "Şube Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "Şube Status")
    private Boolean status;

}
