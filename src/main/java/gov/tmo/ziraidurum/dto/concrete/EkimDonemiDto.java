package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.Year;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Ekim Dönemi Data Transfer Object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EkimDonemiDto implements IDto {

    @ApiModelProperty(required = true,value = "Ekim Dönemi ID")
    private Long id;

    @ApiModelProperty(required = true,value = "Ekim Adı")
    @NotNull
    private String ekimAdi;

    @ApiModelProperty(required = true,value = "Ekim Açıklaması")
    @NotNull
    private String aciklama;

//    @ApiModelProperty(required = true,value = "Ekim Dönemi Created Date")
//    private Date createdAt;
//
//    @ApiModelProperty(required = true,value = "Ekim Dönemi Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "Ekim Dönemi Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "Ekim Dönemi Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "Ekim Dönemi Status")
    private Boolean status;

    @ApiModelProperty(required = true,value = "Year")
    private Year year;

    @ApiModelProperty(required = true,value = "Year Id")
    @NotNull
    private Long yearId;

}
