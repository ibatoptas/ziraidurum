package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Urun Baslik  Transfer Object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrunBaslikSaveDto {

    private Long urunId;
    private List<Long> ids;
    private List<AddedBaslikDto> added;

}
