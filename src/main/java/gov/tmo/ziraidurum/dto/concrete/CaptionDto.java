package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.Baslik;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Caption Data Transfer Object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CaptionDto implements IDto {
    private String ad;
    private String birim;
    private Byte satir;
    private Byte sutun;

}
