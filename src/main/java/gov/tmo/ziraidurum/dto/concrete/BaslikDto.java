package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.enums.ESumAxis;
import gov.tmo.ziraidurum.entities.concrete.enums.ECaptionOrientation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Seviye 1 Data Transfer Object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaslikDto implements IDto {

    @ApiModelProperty(required = true,value = "Başlık Id")
    private Long id;

    @ApiModelProperty(required = true,value = "Başlık Adı")
    @NotNull
    private String ad;

    @ApiModelProperty(required = true,value = "Başlık Yerleşim Tipi")
    @NotNull
    private ECaptionOrientation hiza;

    @ApiModelProperty(required = true,value = "Başlık Toplam Eksen")
    @NotNull
    private ESumAxis sumAxis;
//
//    @ApiModelProperty(required = true,value = "Başlık Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "Başlık Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "Başlık Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "Başlık Status")
    private Boolean status;
}
