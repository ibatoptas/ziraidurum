package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.EkimDonemi;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Zirai Döne Data Transfer Object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZiraiDonemDto implements IDto {

    @ApiModelProperty(required = true,value = "Ekim Dönemi ID")
    private Long id;

    @ApiModelProperty(required = true,value = "Ekim Dönemi Başlangıç Tarihi")
    @NotNull
    private Date baslangicTarihi;

    @ApiModelProperty(required = true,value = "Ekim Dönemi Bitiş Tarihi")
    @NotNull
    private Date bitisTarihi;


    @ApiModelProperty(required = true,value = "Ekim Dönemi Adı")
    @NotNull
    private String donemAdi;

    @ApiModelProperty(required = true,value = "Ekim Dönemi Açıklaması")
    private String aciklama;

//    @ApiModelProperty(required = true,value = "Ekim Ürün Dönemi")
//    private EkimDonemiDto ekimDonemiDto;

    @ApiModelProperty(required = true,value = "Ekim Ürün Dönemi Id")
    @NotNull
    private Long ekimDonemId;

//    @ApiModelProperty(required = true,value = "Ürün")
//    private UrunDto urunDto;

    @ApiModelProperty(required = true,value = "Ürün Id")
    @NotNull
    private Long urunId;

//    @ApiModelProperty(required = true,value = "Ekim Dönemi Created Date")
//    private Date createdAt;
//
//    @ApiModelProperty(required = true,value = "Ekim Dönemi Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "Ekim Dönemi Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "Ekim Dönemi Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "Ekim Dönemi Status")
    private Boolean status;
}
