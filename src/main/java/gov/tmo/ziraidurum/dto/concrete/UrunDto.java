package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Urun Data Transfer Object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrunDto implements IDto {

    @ApiModelProperty(required = true,value = "Ürün ID")
    private Long id;

    @ApiModelProperty(required = true,value = "Ürün Adı")
    @NotNull
    private String urunAdi;

    @ApiModelProperty(required = true,value = "Ürün Kodu")
    @NotNull
    private String urunKodu;

//    @ApiModelProperty(required = true,value = "Ürün Created Date")
//    private Date createdAt;
//
//    @ApiModelProperty(required = true,value = "Ürün Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "Ürün Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "Ürün Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "Ürün Status")
    private Boolean status;
}
