package gov.tmo.ziraidurum.dto.concrete;

import gov.tmo.ziraidurum.entities.concrete.enums.ECaptionOrientation;
import gov.tmo.ziraidurum.entities.concrete.enums.ESumAxis;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Added Baslık Data Transfer Object")
public class AddedBaslikDto {

    private Long id;
    private String ad;
    private ECaptionOrientation hiza;
    private Byte rowCount;
    private Byte colCount;
    private List<CaptionDto> captions;
    private List<FormModelDto> formModels;
    private ESumAxis sumAxis;

}
