package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.Sube;
import gov.tmo.ziraidurum.entities.concrete.enums.OnaySeviyeType;
import gov.tmo.ziraidurum.entities.concrete.enums.ERoleType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "User Data Transfer Object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto implements IDto {

    @ApiModelProperty(required = true,value = "User ID")
    private Long id;

    @ApiModelProperty(required = true,value = "User name")
    @NotNull
    private String username;

    @ApiModelProperty(required = true,value = "User Password")
    @NotNull
    private String password;

    @ApiModelProperty(required = true,value = "User Kimlik Name")
    @NotNull
    private String kimlikname;

    @ApiModelProperty(required = true,value = "User Kimlik Surname")
    @NotNull
    private String kimliksurname;

    @ApiModelProperty(required = true,value = "User Position")
    @NotNull
    private String position;

    @ApiModelProperty(required = true,value = "User email")
    private String email;

    @ApiModelProperty(required = true,value = "User Sicil")
    @NotNull
    private String sicil;

    @ApiModelProperty(required = true,value = "User HasOnay")
    private boolean hasOnay;

    @ApiModelProperty(required = true,value = "User Position")
    private ERoleType roleType;

    @ApiModelProperty(required = true,value = "Onay Seviye Type")
    private OnaySeviyeType onaySeviyeType;

    @ApiModelProperty(required = true,value = "Onay Seviye Type")
    @NotNull
    private Long onaySeviyeTypeId;

    @ApiModelProperty(required = true,value = "Sube")
    private Sube subeUser;

    @ApiModelProperty(required = true,value = "Sube")
    @NotNull
    private Long subeUserId;
//
//    @ApiModelProperty(required = true,value = "User Created Date")
//    private Date createdAt;
//
//    @ApiModelProperty(required = true,value = "User Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "User Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "User Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "User Status")
    private Boolean status;
}
