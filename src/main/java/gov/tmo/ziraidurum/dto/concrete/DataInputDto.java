package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.FormDataInput;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Data Input Data Transfer Object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataInputDto implements IDto {

    @ApiModelProperty(required = true,value = "DataInput ID")
    private Long id;

    @ApiModelProperty(required = true,value = "Form Data Input")
    private FormDataInputDto formDataInputDto;

    @ApiModelProperty(required = true,value = "Form Data Input Id")
    @NotNull
    private Long formDataInputId;

    @ApiModelProperty(required = true,value = "Form Model Id")
    @NotNull
    private Long formModelId;

    @ApiModelProperty(required = true,value = "Form Model")
    private FormModelDto formModelDto;

    @ApiModelProperty(required = true,value = "Data")
    @NotNull
    private String data;

//    @ApiModelProperty(required = true,value = "Group Date")
//    private Date createdAt;
//
//    @ApiModelProperty(required = true,value = "Group Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "Group Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "Group Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "Group Status")
    private Boolean status;
}
