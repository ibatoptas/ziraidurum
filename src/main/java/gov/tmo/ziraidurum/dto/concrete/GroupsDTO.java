package gov.tmo.ziraidurum.dto.concrete;

import lombok.Getter;
import lombok.Setter;

/**
 * Created By ibrahim.toptas
 * Created At 13.10.2021 - 15:38
 **/
@Getter
@Setter
public class GroupsDTO {
    private String ad;
    private Byte index;
}
