package gov.tmo.ziraidurum.dto.concrete;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * Created By ibrahim.toptas
 * Created At 20.10.2021 - 11:09
 **/
@Getter
@Setter
public class LoginDto {
    @NotEmpty
    private String username;
    @NotEmpty
    private String password;
}
