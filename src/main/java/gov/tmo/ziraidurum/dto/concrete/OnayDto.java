package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.FormDataInput;
import gov.tmo.ziraidurum.entities.concrete.User;
import gov.tmo.ziraidurum.entities.concrete.enums.OnayDurum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Group Data Transfer Object")
public class OnayDto implements IDto {

    @ApiModelProperty(required = true,value = "Onay ID")
    private Long id;

    @ApiModelProperty(required = true,value = "User")
    private User user;

    @ApiModelProperty(required = true,value = "User Id")
    @NotNull
    private Long userId;

    @ApiModelProperty(required = true,value = "FormDataInput")
    private FormDataInput formDataInput;

    @ApiModelProperty(required = true,value = "User Id")
    @NotNull
    private Long formDataInputId;

    @ApiModelProperty(required = true,value = "User Signature")
    @NotNull
    private UUID signature;

    @ApiModelProperty(required = true,value = "Onay Durum")
    @NotNull
    private OnayDurum onayDurum;

    @ApiModelProperty(required = true,value = "Açıklama")
    private String aciklama;

//    @ApiModelProperty(required = true,value = "Onay Created Date")
//    private Date createdAt;
//
//    @ApiModelProperty(required = true,value = "Onay Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "Onay Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "Onay Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "Onay Status")
    private Boolean status;
}
