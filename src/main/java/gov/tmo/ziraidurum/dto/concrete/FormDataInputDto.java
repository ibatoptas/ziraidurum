package gov.tmo.ziraidurum.dto.concrete;


import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Form Data Input Transfer Object")
public class FormDataInputDto implements IDto {

    @ApiModelProperty(required = true,value = "Form Model ID")
    private Long id;

    @ApiModelProperty(required = true,value = "Şube")
    private SubeDto subeDto;

    @ApiModelProperty(required = true,value = "Şube Id")
    @NotNull
    private Long SubeId;

    @ApiModelProperty(required = true,value = "Zirai Dönem Id")
    @NotNull
    private Long ZiraiDonemId;

    @ApiModelProperty(required = true,value = "Zirai Dönem")
    private ZiraiDonemDto ziraiDonemDto;

    @ApiModelProperty(required = true,value = "Form Açıklama")
    private String formAciklama;

    @ApiModelProperty(required = true,value = "Form Onay Status")
    private EFormOnayStatus formOnayStatus;

//    @ApiModelProperty(required = true,value = "Group Date")
//    private Date createdAt;
//
//    @ApiModelProperty(required = true,value = "Group Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "Group Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "Group Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "Group Status")
    private Boolean status;

}
