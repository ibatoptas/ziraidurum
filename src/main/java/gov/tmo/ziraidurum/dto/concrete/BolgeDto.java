package gov.tmo.ziraidurum.dto.concrete;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Bölge Data Transfer Object")
@JsonIgnoreProperties(ignoreUnknown = true)
public class BolgeDto implements IDto {

    @ApiModelProperty(required = true,value = "Bölge ID")
    private Long id;

    @ApiModelProperty(required = true,value = "Bölge Adı")
    @NotNull
    private String name;

//    @ApiModelProperty(required = true,value = "Bölge Date")
//    private Date createdAt;
//
//    @ApiModelProperty(required = true,value = "Bölge Created By")
//    private String createdBy;
//
//    @ApiModelProperty(required = true,value = "Bölge Updated Date")
//    private Date updatedAt;
//
//    @ApiModelProperty(required = true,value = "Bölge Updated By")
//    private String updatedBy;

    @ApiModelProperty(required = true,value = "Bölge Status")
    private Boolean status;


}
