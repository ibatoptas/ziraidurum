package gov.tmo.ziraidurum.dto.abstacts.projections;

import java.util.List;

public interface IPFormModel {

    Long getId();
    Integer getDataType();
    Byte getSatir();
    Byte getSutun();
    List<IPGroup> getGroups();
}
