package gov.tmo.ziraidurum.dto.abstacts.projections;

import org.springframework.beans.factory.annotation.Value;

/**
 * Created by iBa at 16.10.2021 00:08
 */
public interface IPDataInput {
    Long getId();

    String getData();

    @Value("#{target.formModel.id}")
    Long getFormModelId();

//    FormModelInfo getFormModel();
//
//    interface FormModelInfo {
//        Long getId();
//    }
}
