package gov.tmo.ziraidurum.dto.abstacts.projections;

public interface BolgeInfo {
    Long getId();

    String getName();
}
