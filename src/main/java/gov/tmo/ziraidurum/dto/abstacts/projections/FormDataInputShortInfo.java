package gov.tmo.ziraidurum.dto.abstacts.projections;

import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;

public interface FormDataInputShortInfo {
    Long getId();

    String getFormAciklama();

    EFormOnayStatus getFormOnayStatus();
}
