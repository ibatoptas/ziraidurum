package gov.tmo.ziraidurum.dto.abstacts.projections;

public interface YearInfo {

    Long getId();

    String getYearName();
}
