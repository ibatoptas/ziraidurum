package gov.tmo.ziraidurum.dto.abstacts.projections;

public interface IPAdId {
    Long getId();
    String getAd();
}
