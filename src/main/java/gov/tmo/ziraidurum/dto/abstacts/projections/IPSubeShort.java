package gov.tmo.ziraidurum.dto.abstacts.projections;

/**
 * Created By ibrahim.toptas
 * Created At 18.10.2021 - 14:52
 **/
public interface IPSubeShort {
    Long getId();

    String getSubeAdi();
}
