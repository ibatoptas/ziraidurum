package gov.tmo.ziraidurum.dto.abstacts.projections;

import org.springframework.beans.factory.annotation.Value;

public interface EkimDonemiInfo {
    Long getId();

    String getAciklama();

    String getEkimAdi();

    @Value("#{target.year.yearName}")
    String getEkimYili();


}
