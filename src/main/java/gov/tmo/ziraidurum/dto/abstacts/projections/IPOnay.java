package gov.tmo.ziraidurum.dto.abstacts.projections;

import gov.tmo.ziraidurum.entities.concrete.enums.OnayDurum;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

/**
 * Created by iBa at 19.10.2021 19:51
 */
public interface IPOnay {
    Long getId();

    Date getUpdatedAt();

    Integer getOnayAction();

    OnayDurum getOnayDurum();

    default String getOnayStr() {
        return getOnayDurum().getText();
    }


    @Value("#{target.user.adSoyad}")
    String getAdSoyad();
}
