package gov.tmo.ziraidurum.dto.abstacts.projections;

import java.util.List;

public interface IPBaslik {
    Long getId();

    String getAd();

    Integer getHiza();

    Integer getSumAxis();

    List<IPCaption> getCaptions();

    List<IPFormModel> getFormModels();
}
