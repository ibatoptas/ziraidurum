package gov.tmo.ziraidurum.dto.abstacts.projections;

import gov.tmo.ziraidurum.entities.concrete.*;
import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;
import gov.tmo.ziraidurum.entities.concrete.enums.OnayDurum;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

public interface FormDataInputInfo {
    Long getId();

    String getFormAciklama();

    EFormOnayStatus getFormOnayStatus();

    Boolean isStatus();

    Set<OnayInfo> getOnays();

    SubeInfo getSube();

    ZiraiDonemInfo getZiraiDonem();

    interface OnayInfo {
        Long getId();

        String getAciklama();

        FormDataInput getFormDataInput();

        OnayDurum getOnayDurum();

        UUID getSignature();

        Boolean isStatus();

        User getUser();
    }

    interface SubeInfo {
        Long getId();

        Bolge getBolge();

        String getSubeAdi();

        String getSubeKodu();
    }

    interface ZiraiDonemInfo {
        Long getId();

        String getAciklama();

        Date getBaslangicTarihi();

        Date getBitisTarihi();

        EkimDonemi getEkimDonemi();

        Urun getUrun();
    }
}
