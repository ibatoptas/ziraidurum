package gov.tmo.ziraidurum.dto.abstacts.projections;

public interface IPGroup {

    Long getId();
    String getAd();
    Byte getIndex();
}
