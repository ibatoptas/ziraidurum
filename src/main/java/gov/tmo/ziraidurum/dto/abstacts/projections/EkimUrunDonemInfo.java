package gov.tmo.ziraidurum.dto.abstacts.projections;

public interface EkimUrunDonemInfo {
    Long getId();

    EkimDonemiInfo getEkimDonemiUrun();

    UrunInfo getUrunDonem();

    interface EkimDonemiInfo {
        Long getId();

        String getEkimAdi();

        String getEkimYili();
    }

    interface UrunInfo {
        Long getId();

        String getUrunAdi();

        String getUrunKodu();
    }
}
