package gov.tmo.ziraidurum.dto.abstacts.projections;

import gov.tmo.ziraidurum.entities.concrete.Sube;
import gov.tmo.ziraidurum.entities.concrete.ZiraiDonem;
import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;
import gov.tmo.ziraidurum.entities.concrete.enums.OnayDurum;

import java.util.Date;
import java.util.UUID;

public interface OnayInfo {
    Long getId();

    String getAciklama();

    Date getCreatedAt();

    OnayDurum getOnayDurum();

    UUID getSignature();

    FormDataInputInfo getFormDataInput();

    UserInfo getUser();

    interface FormDataInputInfo {
        Long getId();

        EFormOnayStatus getFormOnayStatus();

        Sube getSube();

        ZiraiDonem getZiraiDonem();
    }

    interface UserInfo {
        Long getId();

        String getKimlikname();

        String getKimliksurname();

        String getSicil();
    }
}
