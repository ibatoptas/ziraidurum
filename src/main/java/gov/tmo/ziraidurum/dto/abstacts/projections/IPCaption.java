package gov.tmo.ziraidurum.dto.abstacts.projections;

public interface IPCaption {

    Long getId();
    String getAd();
    Byte getSatir();
    Byte getSutun();
}
