package gov.tmo.ziraidurum.dto.abstacts.projections;

import java.util.List;

public interface BaslikInfo {
    Long getId();

    String getAd();

    Integer getHiza();

    Integer getSumAxis();

    List<CaptionInfo> getCaptions();

    List<FormModelInfo> getFormModels();

    interface CaptionInfo {
        String getAd();

        String getBirim();

        Byte getSatir();

        Byte getSutun();
    }

    interface FormModelInfo {
        Long getId();

        Integer getDataType();

        Byte getSatir();

        List<GroupInfo> getGroups();

        Byte getSutun();
    }
    interface GroupInfo{
        String getAd();
        Byte getIndex();
    }
}
