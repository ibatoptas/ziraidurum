package gov.tmo.ziraidurum.dto.abstacts.projections;

/**
 * Created By ibrahim.toptas
 * Created At 14.10.2021 - 12:26
 **/
public interface IPEkimDonemiShort {
    Long getId();

    String getEkimAdi();
}
