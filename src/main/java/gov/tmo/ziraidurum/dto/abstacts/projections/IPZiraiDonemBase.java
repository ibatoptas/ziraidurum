package gov.tmo.ziraidurum.dto.abstacts.projections;

import java.util.Date;

/**
 * Created by iBa at 15.10.2021 13:57
 */
public interface IPZiraiDonemBase {
    Long getId();

    String getAciklama();

    Date getBaslangicTarihi();

    Date getBitisTarihi();

    String getDonemAdi();
}
