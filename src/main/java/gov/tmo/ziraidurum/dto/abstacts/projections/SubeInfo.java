package gov.tmo.ziraidurum.dto.abstacts.projections;

import gov.tmo.ziraidurum.dto.abstacts.IDto;

/**
 * Created By ibrahim.toptas
 * Created At 5.10.2021 - 11:18
 **/
public interface SubeInfo extends IDto {
    Long getId();

    Boolean isStatus();

    String getSubeAdi();

    String getSubeKodu();

    BolgeInfo getBolge();


    interface BolgeInfo {
        Long getId();

        String getName();
    }

}
