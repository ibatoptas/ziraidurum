package gov.tmo.ziraidurum.dto.abstacts.projections;

public interface UrunInfo {
    Long getId();

    String getUrunAdi();

    String getUrunKodu();
}
