package gov.tmo.ziraidurum.dto.abstacts.projections;

import java.util.Date;

public interface ZiraiDonemInfo {
    Long getId();

    String getAciklama();

    Date getBaslangicTarihi();

    Date getBitisTarihi();

    String getDonemAdi();

    String getDonemYili();
}
