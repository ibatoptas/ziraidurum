package gov.tmo.ziraidurum.dto.abstacts.projections;

import org.springframework.beans.factory.annotation.Value;

import java.util.Set;

/**
 * Created by iBa at 16.10.2021 00:09
 */
public interface IPFormDataInput {
    Long getId();

    Integer getFormOnayStatus();

    Set<IPBaslik> getBasliks();

    Set<IPDataInput> getDataInputs();

    Set<IPOnay> getOnays();

    @Value("#{@utilBeans.isFormDataInputReadonly(target)}")
    boolean getReadonly();


}
