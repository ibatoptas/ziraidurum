package gov.tmo.ziraidurum.entities.concrete.enums;

public enum OnaySeviyeType {
    Seviye_0,
    Seviye_1,
    Seviye_2,
    Seviye_3,
    Seviye_4
}
