package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonBackReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "urun", indexes = {@Index(name = "idx_urunkodu", columnList = "urun_kodu")})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Urun extends BaseEntity implements IEntity {

    @Column(name="urun_adi",length = 100, nullable = false)
    private String urunAdi;

    @Column(name="urun_kodu",length = 8, nullable = false)
    private String urunKodu;

    @ManyToMany
    @JoinTable(name = "URUN_BASLIK",
            joinColumns = @JoinColumn(name = "URUN_id"),
            inverseJoinColumns = @JoinColumn(name = "BASLIK_id"))
    private Set<Baslik> basliks;

    @OneToMany(mappedBy="urun")
    @JsonBackReference
    private Set<ZiraiDonem> urun;

    @OneToMany(mappedBy="ekimDonemi")
    @JsonBackReference
    private Set<ZiraiDonem> ekimDonemi;

}