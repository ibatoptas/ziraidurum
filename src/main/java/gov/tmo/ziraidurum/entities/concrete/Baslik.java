package gov.tmo.ziraidurum.entities.concrete;

import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import gov.tmo.ziraidurum.entities.concrete.enums.ESumAxis;
import gov.tmo.ziraidurum.entities.concrete.enums.ECaptionOrientation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "baslik")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Baslik extends BaseEntity implements IEntity {

    @Column(name="seviye_adi",length = 80, nullable = false)
    private String ad;

    @Column(name ="yerlesim_type")
    @Enumerated(EnumType.STRING)
    private ECaptionOrientation hiza;

    @Column(name ="toplam_eksen")
    @Enumerated(EnumType.STRING)
    private ESumAxis sumAxis;

    @OneToMany(mappedBy = "baslik",cascade = CascadeType.PERSIST)
    private List<Caption> captions;

    @OneToMany(mappedBy = "baslik",cascade = CascadeType.PERSIST)
    private List<FormModel> formModels;

    @ManyToMany(mappedBy = "basliks")
    private List<Urun> uruns;

    @ManyToMany(mappedBy = "basliks")
    private Set<FormDataInput> formDataInputs;

    @PrePersist
    public void prePersist() {
        if (sumAxis==null) sumAxis = ESumAxis.NONE;
    }

}
