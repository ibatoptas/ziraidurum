package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonBackReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "years")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Year extends BaseEntity implements IEntity {

    @Column(name="year_name",length = 4, nullable = false)
    private String yearName;

    @OneToMany(mappedBy="year")
    @JsonBackReference
    private Set<EkimDonemi> ekimDonemis;
}
