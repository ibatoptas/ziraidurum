package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "data_input")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataInput extends BaseEntity implements IEntity {

    @JoinColumn(name="formdatainput_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(optional=true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private FormDataInput formDataInput;

    @ManyToOne
    @JoinColumn(name = "form_model_id")
    private FormModel formModel;

    @Column(name ="input_one")
    private String data;

}
