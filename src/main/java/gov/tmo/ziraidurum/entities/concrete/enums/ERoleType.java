package gov.tmo.ziraidurum.entities.concrete.enums;

public enum ERoleType {
    TASRA,
    MERKEZ,
    ADMIN
}
