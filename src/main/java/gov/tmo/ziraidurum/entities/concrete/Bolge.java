package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonBackReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "bolgeler")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Bolge extends BaseEntity implements IEntity {

    @Column(name ="bolge_adi", nullable = false, length = 50)
    private String name;

    @OneToMany(mappedBy="bolge")
    @JsonBackReference
    private Set<Sube> subeler;

}
