package gov.tmo.ziraidurum.entities.concrete.enums;

/**
 * Aşağıdaki sıralamayı değiştirme. Bütün onay mantığı bu sıralama üzerine çalışıyor
 */
public enum EFormOnayStatus {
    GIRIS, SEF, MUDUR_YRD, MUDUR, MERKEZ
}
