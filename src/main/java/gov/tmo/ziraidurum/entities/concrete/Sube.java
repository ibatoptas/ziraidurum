package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "subeler", indexes = {@Index(name="idx_subekodu", columnList="sube_kodu")})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Sube extends BaseEntity implements IEntity {

    @Column(name="sube_kodu",length = 20, unique = true, nullable = false)
    private String subeKodu;

    @Column(name="sube_adi", nullable = false, length = 50)
    private String subeAdi;

    @JoinColumn(name="bolge_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(optional=true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private Bolge bolge;

    @OneToMany(mappedBy="subeUser")
    @JsonBackReference
    private Set<User> users;

    @OneToMany(mappedBy="sube")
    @JsonBackReference
    private Set<FormDataInput> formDataInputs;

}
