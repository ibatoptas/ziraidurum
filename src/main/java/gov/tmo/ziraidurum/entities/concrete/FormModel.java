package gov.tmo.ziraidurum.entities.concrete;

import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import gov.tmo.ziraidurum.entities.concrete.enums.EDataType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Table(name = "form_model")
@Entity
@Getter
@Setter
public class FormModel extends BaseEntity implements IEntity {

    @Column(name = "birim", length = 5)
    private String birim;

    @Column(name = "satir", nullable = false)
    private Byte satir;

    @Enumerated(EnumType.STRING)
    private EDataType dataType;

    @ManyToOne
    @JoinColumn(name = "baslik_id")
    private Baslik baslik;

    @Column(name = "sutun")
    private Byte sutun;

    @OneToMany(mappedBy = "formModel")
    private List<Groups> groups;

    @OneToMany(mappedBy = "formModel")
    private List<DataInput> dataInputs;
}
