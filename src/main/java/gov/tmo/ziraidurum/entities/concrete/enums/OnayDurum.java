package gov.tmo.ziraidurum.entities.concrete.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum OnayDurum {
    ONAY("Onay"),
    RED("Red");
    private final String text;
}
