package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;
import gov.tmo.ziraidurum.entities.concrete.enums.ERoleType;
import gov.tmo.ziraidurum.entities.concrete.enums.OnaySeviyeType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;


@Table(name = "users", indexes = {@Index(name = "idx_username", columnList = "uname")})
@Getter
@Setter
@Entity
public class User extends BaseEntity implements IEntity {

    @Column(name="uname", length = 100, unique = true, nullable = false)
    private String username;

    @Column(name="pwd")
    private String password;

    @Column(name = "ad_soyad")
    private String adSoyad;

    @Column(name = "kimlik_name", nullable = false)
    private String kimlikname;

    @Column(name = "kimlik_surname", nullable = false)
    private String kimliksurname;

    @Column(name = "position", nullable = false)
    private String position;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "sicil_number", length = 5)
    private String sicil;

    @Column(name = "has_onay", nullable = false)
    private boolean hasOnay;

    @Column(name = "role_type")
    @Enumerated(EnumType.STRING)
    private ERoleType roleType;

    @Column(name = "onayseviye_type")
    @Enumerated(EnumType.STRING)
    private OnaySeviyeType onaySeviyeType;

    @Column(name = "onay_level")
    @Enumerated(EnumType.STRING)
    private EFormOnayStatus onayLevel;

    @JoinColumn(name = "sube_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private Sube subeUser;

    @OneToMany(mappedBy = "user")
    @JsonBackReference
    private Set<Onay> onays;
}
