package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "caption", indexes = {@Index(name="idx_caption", columnList="seviye_adi")})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Caption extends BaseEntity implements IEntity {

    @Column(name="seviye_adi",length = 80, nullable = false)
    private String ad;

    @Column(name="satir", nullable = false)
    private byte satir;

    @Column(name="sütün", nullable = false)
    private byte sutun;

    @ManyToOne
    @JoinColumn(name = "baslik_id")
    private Baslik baslik;
}
