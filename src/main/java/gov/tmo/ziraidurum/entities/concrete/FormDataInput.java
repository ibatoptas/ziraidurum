package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "form_datainput",uniqueConstraints = {@UniqueConstraint(columnNames = {"ziraidonem_id","sube_id"})})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FormDataInput extends BaseEntity implements IEntity {

    @JoinColumn(name="sube_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(optional=true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private Sube sube;

    @JoinColumn(name="ziraidonem_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(optional=true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private ZiraiDonem ziraiDonem;

    @Column(name = "form_aciklama", columnDefinition = "TEXT")
    private String formAciklama;

    @Column(name ="formonay_status")
    @Enumerated(EnumType.ORDINAL)
    private EFormOnayStatus formOnayStatus;

    @OneToMany(mappedBy="formDataInput")
    @JsonBackReference
    private Set<Onay> onays;

    @OneToMany(mappedBy="formDataInput")
    @JsonBackReference
    private Set<DataInput> dataInputs;


    @ManyToMany
    @JoinTable(name = "FORM_DATA_INPUT_BASLIK",
            joinColumns = @JoinColumn(name = "FORM_DATA_INPUT_id"),
            inverseJoinColumns = @JoinColumn(name = "BASLIK_id"))
    private Set<Baslik> basliks;

    @PrePersist
    public void prePersist() {
        if (formOnayStatus==null) formOnayStatus = EFormOnayStatus.GIRIS;
    }
}
