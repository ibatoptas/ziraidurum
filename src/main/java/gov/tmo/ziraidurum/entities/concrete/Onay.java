package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;
import gov.tmo.ziraidurum.entities.concrete.enums.OnayDurum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "onaylar", uniqueConstraints = @UniqueConstraint(columnNames = {"onay_action", "formdatainput_id"}))
@Getter
@Setter
public class Onay extends BaseEntity implements IEntity {

    @JoinColumn(name="user_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(optional=true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private User user;

    @JoinColumn(name="formdatainput_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(optional=true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private FormDataInput formDataInput;

    @GeneratedValue
    @Column(name = "signature")
    private UUID signature;

    @Column(name ="onay_durum")
    @Enumerated(EnumType.STRING)
    private OnayDurum onayDurum;

    @Column(name = "onay_action")
    @Enumerated(EnumType.STRING)
    private EFormOnayStatus onayAction;

    @Column(name = "aciklama", columnDefinition = "TEXT")
    private String aciklama;

}
