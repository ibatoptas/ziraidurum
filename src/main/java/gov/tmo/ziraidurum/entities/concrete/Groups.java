package gov.tmo.ziraidurum.entities.concrete;

import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "groups")
@Entity
@Getter
@Setter
public class Groups extends BaseEntity implements IEntity {

    @Column(name = "ad", nullable = false, length = 40)
    private String ad;

    @Column(name = "index")
    private Byte index;

    @ManyToOne
    @JoinColumn(name = "form_model_id")
    private FormModel formModel;
}
