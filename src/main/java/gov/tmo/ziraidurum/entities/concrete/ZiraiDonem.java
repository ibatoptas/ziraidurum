package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "zirai_donem", indexes = {@Index(name="idx_donemadi", columnList="donem_adi")})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ZiraiDonem extends BaseEntity implements IEntity {

    @Column(name ="baslangic_tarihi")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date baslangicTarihi;

    @Column(name ="bitis_tarihi")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date bitisTarihi;

    @Column(name ="donem_adi", nullable = false , length = 50)
    private String donemAdi;

    @Column(name = "aciklama", columnDefinition = "TEXT")
    private String aciklama;

    @JoinColumn(name="urun_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(optional=true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private Urun urun;

    @JoinColumn(name="ekimdonem_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(optional=true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private EkimDonemi ekimDonemi;

    @OneToMany(mappedBy = "ziraiDonem")
    private List<FormDataInput> formDataInputs;

}
