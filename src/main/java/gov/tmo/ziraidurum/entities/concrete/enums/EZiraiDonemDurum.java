package gov.tmo.ziraidurum.entities.concrete.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created By ibrahim.toptas
 * Created At 14.10.2021 - 15:00
 **/
@RequiredArgsConstructor
@Getter
public enum EZiraiDonemDurum {
    AKTIF("Aktif Dönem"),BASLAMADI("Gelecek Dönem"),BITTI("Geçmiş Dönem");
    private final String text;
}
