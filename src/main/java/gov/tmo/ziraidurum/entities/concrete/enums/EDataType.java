package gov.tmo.ziraidurum.entities.concrete.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Date;

@Getter
@RequiredArgsConstructor
public enum EDataType {
    GROUP(Byte.class),TEXT(String.class),DOUBLE(Long.class),PERCENT(Double.class),DATE(Date.class)
    ;

    private final Class<?> tip;
}
