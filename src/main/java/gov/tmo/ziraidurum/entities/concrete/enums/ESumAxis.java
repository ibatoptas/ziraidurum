package gov.tmo.ziraidurum.entities.concrete.enums;

public enum ESumAxis {
    NONE,HORIZONTAL,VERTICAL,BOTH
}
