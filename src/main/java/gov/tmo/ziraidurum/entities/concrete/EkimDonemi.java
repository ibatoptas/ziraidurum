package gov.tmo.ziraidurum.entities.concrete;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "ekim_donem", indexes = {@Index(name="idx_ekimadi", columnList="ekim_adi")})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EkimDonemi extends BaseEntity implements IEntity {

    @Column(name ="ekim_adi", nullable = false ,length = 100)
    private String ekimAdi;

    @JoinColumn(name="year_id", nullable = false, referencedColumnName = "id")
    @ManyToOne(optional=true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private Year year;

    @Column(name = "explain", columnDefinition = "TEXT")
    private String aciklama;



}
