package gov.tmo.ziraidurum.config.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created By ibrahim.toptas
 * Created At 4.08.2021 - 09:53
 **/
@Component
@RequiredArgsConstructor
public class JwtUtilImpl implements JwtUtil {
    private final HttpServletRequest request;
    private final ObjectMapper mapper;

    @Value("${jwt.secret}")
    private String jwtSecret;
    @Value("${jwt.expiration}")
    private int jwtExpiration;

    @Override
    public String generateToken(Authentication authentication) throws JsonProcessingException {
        LoggedUserModel user = (LoggedUserModel) authentication.getPrincipal();
        return generateToken(user);
    }

    @Override
    public LoggedUserModel getAuthenticationFromToken(String token) throws JsonProcessingException {
        String readToken = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token)
                .getBody().getSubject();
        return mapper.readValue(readToken, LoggedUserModel.class);
    }

    @Override
    public boolean validateToken(String token) throws JsonProcessingException {
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
        LoggedUserModel loggedUserModel = mapper.readValue(claimsJws.getBody().getSubject(), LoggedUserModel.class);
        return true;
    }

    @Override
    public String refreshToken() throws JsonProcessingException {
        return generateToken(request);
    }

    @Override
    public String generateToken(LoggedUserModel logged) throws JsonProcessingException {
        Map<String, Object> extraInfo = new HashMap<>();
        extraInfo.put("ip", request.getRemoteAddr());

        Date date = new Date();
        return Jwts.builder()
                .setSubject(mapper.writeValueAsString(logged))
                .addClaims(extraInfo)
                .setIssuedAt(date)
                .setExpiration(new Date(date.getTime() + jwtExpiration))
                .setIssuer("Tmo Geçici Görev")
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    private String generateToken(HttpServletRequest request) throws JsonProcessingException {
        String currentToken = request.getHeader("Authorization").substring(7);
        if (validateToken(currentToken)) {
            LoggedUserModel logged = getAuthenticationFromToken(currentToken);
            return generateToken(logged);
        }
        return null;
    }
}
