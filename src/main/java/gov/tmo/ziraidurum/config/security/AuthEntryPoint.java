package gov.tmo.ziraidurum.config.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created By ibrahim.toptas
 * Created At 4.08.2021 - 10:37
 **/
@Component
@RequiredArgsConstructor
public class AuthEntryPoint implements AuthenticationEntryPoint {
//    private final MessageSourceAccessor message;

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {

        httpServletResponse.setContentType("application/json;charset=UTF-8");
        httpServletResponse.getWriter().write("Login Fail");
    }
}
