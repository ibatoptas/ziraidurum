package gov.tmo.ziraidurum.config.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created By ibrahim.toptas
 * Created At 4.08.2021 - 10:37
 **/
@Component
@RequiredArgsConstructor
public class AccessDeniedFilter implements AccessDeniedHandler {
    //    private final MessageSourceAccessor message;
    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {

        httpServletResponse.getWriter().write("Access Denied");
    }
}
