package gov.tmo.ziraidurum.config.security;

import gov.tmo.ziraidurum.entities.concrete.User;
import gov.tmo.ziraidurum.repository.concrete.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created By ibrahim.toptas
 * Created At 4.08.2021 - 10:17
 **/
@RequiredArgsConstructor
@Component
public class LdapAuthenticationProvider implements AuthenticationProvider {
    //    private final MessageSourceAccessor message;
    private final UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username;
        String password = "";
        LoggedUserModel userModel;
        boolean fromToken = false;
        if (authentication instanceof LoggedUserModel) {
            username = ((LoggedUserModel) authentication).getUsername();
            fromToken = true;
        } else {
            username = (String) authentication.getPrincipal();
            password = (String) authentication.getCredentials();
        }
        User userEntity = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Kullanıcı adı veya şifre hatalı"));

        if (!fromToken) {
            if (!password.equals("1")) throw new UsernameNotFoundException("Kullanıcı adı ve şifre hatalı");
            Long subeId = null;
            String subeAd = "Merkez";
            if (userEntity.getSubeUser() != null) {
                subeId = userEntity.getSubeUser().getId();
                subeAd = userEntity.getSubeUser().getSubeAdi();
            }
            userModel = new LoggedUserModel(userEntity);
        } else {
            userModel = (LoggedUserModel) authentication.getPrincipal();
        }
        return new UsernamePasswordAuthenticationToken(userModel, null);
//        User entity = userRepository.findByUsername(username)
//                .orElseThrow(() -> {
//                    throw new UsernameNotFoundException(message.getMessage("login.failed"));
//                });
//
////        if (!fromToken && password.equals("1") &&
////                (username.equals("ibrahim.toptas")
////                        || username.equals("gokhan.unlu")
////                        || username.equals("firat.demircan")
////                        || username.equals("merkez")
////                        || username.equals("ankara")
////                        || username.equals("eskisehir")
////                        || username.equals("aksaray")
////                        || username.equals("kirsehir")
////                        || username.equals("diyarbakir")
////
////                )){
////
////            Isyeri isy = entity.getIsyeri();
////            userModel = new LoggedUserModel(username, "Test Kullanıcı", isy != null ? isy.getId() : null, isy!=null ? isy.getAd() :"TEST", entity.getRole());
////
////            return new UsernamePasswordAuthenticationToken(userModel, password, Arrays.asList(new SimpleGrantedAuthority(userModel.getRole().name())));
////
////        }
//
//        if (!fromToken) {
//            try {
//                Long subeId;
//                if (entity.getRole().equals(EUserRole.ROLE_ADMIN) && password.indexOf("-")>-1){
//                    String[] spl = password.split("-");
//                    password = spl[0];
//                    subeId = Long.valueOf(spl[1]);
//                }else{
//                    subeId = entity.getIsyeri() == null ? null : entity.getIsyeri().getId();
//                }
//                LdapAuthenticator authenticator = LdapAuthenticator.getInstance();
//                String displayName = authenticator.authenticate(username, password);
//                userModel = new LoggedUserModel(entity.getId(), username, displayName,
//                        subeId,
//                        entity.getIsyeri()==null ?"Merkez":entity.getIsyeri().getAd(),entity.getRole());
//            } catch (NamingException e) {
//                throw new UsernameNotFoundException(message.getMessage("login.failed"));
//            }
//        } else {
//            userModel = (LoggedUserModel) authentication.getPrincipal();
//        }
//        if (entity.getLocked()){
//            throw new InsufficientAuthenticationException(message.getMessage("user.locked"));
//        }
//
//        return new UsernamePasswordAuthenticationToken(userModel, password, Arrays.asList(new SimpleGrantedAuthority(userModel.getRole().name())));
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }

}
