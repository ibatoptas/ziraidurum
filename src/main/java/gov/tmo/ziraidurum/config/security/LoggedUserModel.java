package gov.tmo.ziraidurum.config.security;

import gov.tmo.ziraidurum.entities.concrete.User;
import gov.tmo.ziraidurum.entities.concrete.enums.ERoleType;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created By ibrahim.toptas
 * Created At 3.08.2021 - 12:35
 **/
@Data
@AllArgsConstructor
public class LoggedUserModel {
    private Long userId;
    private String username;
    private String displayName;
    private Long subeId;
    private String subeAd;
    private Integer onayLevel;
    private ERoleType roleType;

    public LoggedUserModel() {
    }

    public LoggedUserModel(User user) {
        this.userId = user.getId();
        this.username = user.getUsername();
        this.displayName = user.getAdSoyad();
        if (user.getSubeUser() != null) {
            this.subeId = user.getSubeUser().getId();
            this.subeAd = user.getSubeUser().getSubeAdi();
        } else {
            this.subeId = null;
            this.subeAd = "Merkez";
        }
        this.onayLevel = user.getOnayLevel() != null ? user.getOnayLevel().ordinal() : -1;
        this.roleType = user.getRoleType();
    }

}
