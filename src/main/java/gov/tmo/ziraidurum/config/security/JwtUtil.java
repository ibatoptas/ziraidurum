package gov.tmo.ziraidurum.config.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.security.core.Authentication;

/**
 * Created By ibrahim.toptas
 * Created At 4.08.2021 - 09:52
 **/
public interface JwtUtil {
    String generateToken(Authentication authentication) throws JsonProcessingException;

    LoggedUserModel getAuthenticationFromToken(String token) throws JsonProcessingException;

    boolean validateToken(String token) throws JsonProcessingException;

    String refreshToken() throws JsonProcessingException;

    String generateToken(LoggedUserModel logged) throws JsonProcessingException;
}
