package gov.tmo.ziraidurum.config;

import gov.tmo.ziraidurum.entities.concrete.User;
import gov.tmo.ziraidurum.repository.concrete.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by iBa at 19.10.2021 20:21
 */
@Component
@RequiredArgsConstructor
public class AudAware implements AuditorAware<User> {
    private final UserRepository userRepository;
    @Override
    public Optional<User> getCurrentAuditor() {

        return userRepository.findById(1L);
    }

}
