package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.concrete.CaptionDto;
import gov.tmo.ziraidurum.service.abstacts.CaptionService;
import org.springframework.stereotype.Service;

@Service
public class CaptionServiceImpl implements CaptionService {

    @Override
    public Result save(CaptionDto captionDto) {
        return null;
    }

    @Override
    public Result update(Long id, CaptionDto captionDto) {
        return null;
    }

    @Override
    public Result softDelete(Long id) {
        return null;
    }

    @Override
    public Result delete(Long id) {
        return null;
    }
}
