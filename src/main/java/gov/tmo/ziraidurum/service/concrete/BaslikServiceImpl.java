package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Conctants.Messages;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessDataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.BaslikInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPBaslik;
import gov.tmo.ziraidurum.dto.abstacts.projections.UrunInfo;
import gov.tmo.ziraidurum.dto.concrete.BaslikDto;
import gov.tmo.ziraidurum.repository.concrete.BaslikRepository;
import gov.tmo.ziraidurum.service.abstacts.BaslikService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BaslikServiceImpl implements BaslikService {

    private final BaslikRepository baslikRepository;

    public BaslikServiceImpl(BaslikRepository baslikRepository) {
        this.baslikRepository = baslikRepository;
    }

    @Override
    public Result save(BaslikDto baslikDto) {
        return null;
    }

    @Override
    public Result update(Long id, BaslikDto baslikDto) {
        return null;
    }

    @Override
    public Result softDelete(Long id) {
        return null;
    }

    @Override
    public Result delete(Long id) {
        return null;
    }


    @Override
    public DataResult<List<IPBaslik>> getAll() {
        List<IPBaslik> baslikInfos = baslikRepository.findProjectedBy(IPBaslik.class);
        return new SuccessDataResult<>(baslikInfos, Messages.baslikListed);
    }

    @Override
    public DataResult<List<IPBaslik>> getBasliksByUrunId(Long urunId) {
        List<IPBaslik> ipBasliks = baslikRepository.findProjectedByUruns_Id(IPBaslik.class,urunId);
        return new SuccessDataResult<>(ipBasliks,Messages.baslikListedByUrun);
    }
}
