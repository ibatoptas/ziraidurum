package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Conctants.Messages;
import gov.tmo.ziraidurum.core.Utilities.Results.*;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPAdId;
import gov.tmo.ziraidurum.dto.abstacts.projections.UrunInfo;
import gov.tmo.ziraidurum.dto.concrete.UrunDto;
import gov.tmo.ziraidurum.entities.concrete.Urun;
import gov.tmo.ziraidurum.repository.concrete.UrunRepository;
import gov.tmo.ziraidurum.service.abstacts.mappers.UrunMapper;
import gov.tmo.ziraidurum.service.abstacts.UrunService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UrunServiceImpl implements UrunService {

    private final UrunRepository urunRepository;
    private final UrunMapper urunMapper;


    public UrunServiceImpl(UrunMapper urunMapper, UrunRepository urunRepository) {
        this.urunRepository = urunRepository;
        this.urunMapper = urunMapper;
    }


    @Override
    public Result save(UrunDto urunDto) {

        Urun urunDb = urunRepository.findByUrunKodu(urunDto.getUrunKodu());
        if(urunDb != null)
        {
           return new ErrorResult(Messages.urunCodeError);
        }

        urunDto.setStatus(Boolean.TRUE);
//        urunDto.setCreatedAt(new Date());
        Urun urun = urunMapper.dtoToEntity(urunDto);
        this.urunRepository.save(urun);
        return new SuccessResult(Messages.urunSaved);
    }

    @Override
    public Result update(Long id, UrunDto urunDto) {
        Optional<Urun> urunIs = urunRepository.findById(id);
        Urun urunDb = urunIs.get();

        Urun urunCheck = urunRepository.findByUrunKoduAndIdNot(urunDto.getUrunKodu(),id);
        if(urunCheck != null){
           return new ErrorResult(Messages.urunCodeError);
        }

        urunDb.setUrunAdi(urunDto.getUrunAdi());
        urunDb.setUrunKodu(urunDto.getUrunKodu());
        urunDb.setUpdatedAt(new Date());
        urunRepository.save(urunDb);

        return new SuccessResult(Messages.urunUpdated);
    }

    @Override
    public Result softDelete(Long id) {

        Optional<Urun> urunIs = urunRepository.findById(id);
        Urun urunDb = urunIs.get();

        urunDb.setStatus(Boolean.FALSE);
        urunRepository.save(urunDb);

        return new SuccessResult(Messages.urunDeleted);
    }

    @Override
    public Result delete(Long id) {
        this.urunRepository.deleteById(id);
        return new SuccessResult(Messages.urunDeleted);
    }

    @Override
    public DataResult<List<UrunInfo>> getAll() {
        List<UrunInfo> urunDatas = urunRepository.findProjectedByStatusTrueOrderByUrunKodu(UrunInfo.class);
        return new SuccessDataResult<>(urunDatas,Messages.urunListed);
    }

    @Override
    public DataResult<List<IPAdId>> getUrunList() {
        List<IPAdId> ipAdIds = urunRepository.findProjectedBy(IPAdId.class);
        return new SuccessDataResult<>(ipAdIds,Messages.urunListed);
    }

    @Override
    public DataResult<UrunInfo> getById(Long id) {
        UrunInfo  urunInfo = urunRepository.findProjectedById(UrunInfo.class,id);
        return new SuccessDataResult<>(urunInfo,Messages.urunGetted);
    }

}
