package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.config.security.LoggedUserModel;
import gov.tmo.ziraidurum.entities.concrete.enums.ERoleType;
import gov.tmo.ziraidurum.service.abstacts.SharedService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by iBa at 15.10.2021 23:26
 */
@Component
@RequiredArgsConstructor
public class SharedServiceImpl implements SharedService {
    @Override
    public Long getSubeId() {
        return getLoggedUser().getSubeId();
    }

    @Override
    public ERoleType getUserRole() {
        return getLoggedUser().getRoleType();
    }

    @Override
    public Integer getUserOnayLevel() {
        return getLoggedUser().getOnayLevel();
    }

    @Override
    public LoggedUserModel getLoggedUser() {
        LoggedUserModel result = (LoggedUserModel) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return result;
    }
}
