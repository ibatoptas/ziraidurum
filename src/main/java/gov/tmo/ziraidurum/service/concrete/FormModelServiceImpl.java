package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.config.security.LoggedUserModel;
import gov.tmo.ziraidurum.core.Utilities.Conctants.Messages;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.ErrorResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPBaslik;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPFormDataInput;
import gov.tmo.ziraidurum.dto.concrete.FormModelDto;
import gov.tmo.ziraidurum.dto.concrete.TasraFormSaveDto;
import gov.tmo.ziraidurum.dto.concrete.UrunBaslikSaveDto;
import gov.tmo.ziraidurum.entities.concrete.*;
import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;
import gov.tmo.ziraidurum.entities.concrete.enums.OnayDurum;
import gov.tmo.ziraidurum.repository.concrete.*;
import gov.tmo.ziraidurum.service.abstacts.FormModelService;
import gov.tmo.ziraidurum.service.abstacts.SharedService;
import gov.tmo.ziraidurum.util.Util;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FormModelServiceImpl implements FormModelService {

    private final BaslikRepository baslikRepository;
    private final CaptionRepository captionRepository;
    private final FormModelRepository formModelRepository;
    private final GroupsRepository groupsRepository;
    private final UrunRepository urunRepository;
    private final FormDataInputRepository formDataInputRepository;
    private final ZiraiDonemRepository ziraiDonemRepository;
    private final DataInputRepository dataInputRepository;
    private final OnayRepository onayRepository;
    private final SharedService sharedService;



    @Override
    public Result save(FormModelDto formModelDto) {
        return null;
    }

    @Override
    public Result update(Long id, FormModelDto formModelDto) {
        return null;
    }

    @Override
    public Result softDelete(Long id) {
        return null;
    }

    @Override
    public Result delete(Long id) {
        return null;
    }


    @Override
    public Result saveFormModel(UrunBaslikSaveDto urunBaslikSaveDto) {
        List<Baslik> savedBasliks = new ArrayList<>();
        urunBaslikSaveDto.getAdded().forEach(x -> {
            Baslik baslik = new Baslik();
            baslik.setAd(x.getAd());
            baslik.setHiza(x.getHiza());
            baslik.setStatus(Boolean.TRUE);
            baslik.setSumAxis(x.getSumAxis());
            Baslik savedBaslik = baslikRepository.save(baslik);
            savedBasliks.add(savedBaslik);
            List<Caption> captions = new ArrayList<>();
            x.getCaptions().forEach(capt -> {
                Caption caption = new Caption();
                caption.setAd(capt.getAd());
                caption.setSatir(capt.getSatir());
                caption.setSutun(capt.getSutun());
                caption.setBaslik(savedBaslik);
                caption.setStatus(Boolean.TRUE);
                caption.setCreatedAt(new Date());
                captions.add(caption);
            });
            captionRepository.saveAll(captions);
            x.getFormModels().forEach(fm -> {
                FormModel formModel = new FormModel();
                formModel.setSutun(fm.getSutun());
                formModel.setSatir(fm.getSatir());
                formModel.setDataType(fm.getDataType());
                formModel.setBaslik(savedBaslik);
                formModel.setStatus(Boolean.TRUE);
                FormModel savedFormModel = formModelRepository.save(formModel);
                if (fm.getGroups()!=null){
                    List<Groups> spannedGroups = new ArrayList<>();
                    fm.getGroups().forEach(grp->{
                        Groups spn = new Groups();
                        spn.setIndex(grp.getIndex());
                        spn.setAd(grp.getAd());
                        spn.setStatus(Boolean.TRUE);
                        spn.setFormModel(savedFormModel);
                        spannedGroups.add(spn);
                    });
                    groupsRepository.saveAll(spannedGroups);
                }
            });
        });
        Urun urun = urunRepository.findById(urunBaslikSaveDto.getUrunId()).get();
        urun.getBasliks().clear();
        urun.getBasliks().addAll(savedBasliks);
        urun.getBasliks().addAll(Util.createEntityWidthId( urunBaslikSaveDto.getIds(),Baslik.class));
        urunRepository.save(urun);

        return new SuccessResult(Messages.formModelSaved);
    }

    @Override
    public DataResult<Map<String,Object>> getFormDataInputs(Long ziraiDonemId) {
        Long subeId=sharedService.getSubeId();
        Map<String, Object> result = new HashMap<>();
        boolean hasInput=false;
        if (formDataInputRepository.existsBySube_IdAndZiraiDonem_Id(subeId,ziraiDonemId)){
            hasInput=true;
            IPFormDataInput data = formDataInputRepository.findBySube_IdAndZiraiDonem_Id(IPFormDataInput.class, subeId, ziraiDonemId);
            result.put("result", data);
        }else{
            ZiraiDonem ziraiDonem = ziraiDonemRepository.findById(ziraiDonemId).get();
            List<IPBaslik> data = baslikRepository.findProjectedByUruns_Id(IPBaslik.class, ziraiDonem.getUrun().getId());
            result.put("list",data);
        }
        result.put("hasInput", hasInput);
        return new DataResult<>(result,true);
    }
    @Override
    public Result getFormDataInputsBySube(Long ziraiDonemId, Long subeId) {
        IPFormDataInput result = formDataInputRepository.findBySube_IdAndZiraiDonem_Id(IPFormDataInput.class, subeId, ziraiDonemId);
        return new DataResult<>(result, true);

    }

    @Override
    public Result getFormDataInputsByUrunAndSube(Long donemId, Long urunId, Long subeId) {
        List<ZiraiDonem> ziraiDonems = ziraiDonemRepository.findByEkimDonemi_IdAndUrun_IdOrderByBitisTarihiDesc(ZiraiDonem.class, donemId, urunId);
//        formDataInputRepository.findBySube_IdAndZiraiDonem_Id(IPFormDataInput.class,)
        return null;
    }

    @Override
    public Result setFormDataInputOnay(Boolean act, Long formId) {
        FormDataInput formDataInput = formDataInputRepository.findById(formId).get();
        formDataInput.setFormOnayStatus(act ? EFormOnayStatus.MERKEZ : null);
        formDataInputRepository.save(formDataInput);
        return new SuccessResult(act ? "Form Onaylandı" : "Form Onayı Kaldırıldı");
    }

    @Transactional
    @Override
    public Result updateFormDataInputOnay(Boolean act, Long formDataInputId) {

        LoggedUserModel loggedUser = sharedService.getLoggedUser();
        FormDataInput formDataInput = formDataInputRepository.findById(formDataInputId).get();
        int formOnayLevel = formDataInput.getFormOnayStatus().ordinal();
        if (loggedUser.getOnayLevel() < formOnayLevel) {//kullanıcı leveli form leveliyle eşleşmiyor. ya kullanıcının yetkisi yok yada üst birim onayladı
            return new ErrorResult("Bu işlemi yapma yetkiniz bulunmamaktadır");
        }
        EFormOnayStatus status = EFormOnayStatus.values()[loggedUser.getOnayLevel()];
        if (act) {

            Onay onay = onayRepository.findByUser_IdAndFormDataInput_Id(loggedUser.getUserId(), formDataInputId)
                    .orElse(new Onay());
            onay.setUser(Util.createEntityWithId(loggedUser.getUserId(), User.class));
            onay.setFormDataInput(formDataInput);
            onay.setOnayDurum(OnayDurum.ONAY);
            onay.setOnayAction(status);
            onay.setStatus(true);
            onayRepository.save(onay);
            formDataInput.setFormOnayStatus(status);
        } else {
            onayRepository.deleteByOnayActionAndFormDataInput_Id(status, formDataInputId);
            //onay kaldırıldı onay leveli bir alt levele düştü, eğer şef onayı kaldırdıysa onay level=GIRIS oluyor
            //GIRIS yetkilisine onay tanımlanmayacağı için alt satırın patlama ihtimali yok.
            //ilerde GIRIS'e onay level verilmek istenirse aşağıdaki satır kesinlikle patlar. DIKKAT!!!!!
            formDataInput.setFormOnayStatus(EFormOnayStatus.values()[formOnayLevel - 1]);
        }
        formDataInputRepository.save(formDataInput);
        return new Result(true, "İşlem  başarılı");
    }

    @Override
    public Result saveFormDatas(TasraFormSaveDto dto) {
        FormDataInput formDataInput = assignFormDataInput(dto);
        List<DataInput> dataInputs = new ArrayList<>();
        dto.getDataInputs().forEach((key, val) -> {
            String[] splitted = key.split("_");
            //index=0 boş gelecek çünkü "_" ile başlıyor
            //index=1 formModelId barındırır
            //index=2 daha önce veri girişi varsa dataInputId yoksa sıfır olacak
            Long formModelId = Long.valueOf(splitted[1]);
            Long dataInputId = Long.valueOf(splitted[2]);
            DataInput dataInput = dataInputRepository.findById(dataInputId).orElse(new DataInput());
            dataInput.setData(val);
            dataInput.setFormModel(Util.createEntityWithId(formModelId,FormModel.class));
            dataInput.setFormDataInput(formDataInput);
            dataInputs.add(dataInput);
        });
        dataInputRepository.saveAll(dataInputs);
        return null;
    }



    private FormDataInput assignFormDataInput(TasraFormSaveDto dto) {
        FormDataInput formDataInput;
        Optional<FormDataInput> optFormDataInput = formDataInputRepository.findById(dto.getFormDataInputId());
        if (optFormDataInput.isPresent()){
            formDataInput= optFormDataInput.get();
        }else{
            ZiraiDonem ziraiDonem = ziraiDonemRepository.findById(dto.getZiraiDonemId()).get();

            FormDataInput fd = new FormDataInput();
            fd.setZiraiDonem(ziraiDonem);
            fd.setSube(Util.createEntityWithId(sharedService.getSubeId(),Sube.class));
            List<Long> collect = ziraiDonem.getUrun().getBasliks().stream().map(x -> x.getId()).collect(Collectors.toList());
            fd.setBasliks(Util.createEntityWidthId( collect,Baslik.class).stream().collect(Collectors.toSet()));
            formDataInput = formDataInputRepository.save(fd);
        }
        return formDataInput;
    }
}
