package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.concrete.GroupDto;
import gov.tmo.ziraidurum.service.abstacts.GroupService;
import org.springframework.stereotype.Service;

@Service
public class GroupServiceImpl implements GroupService {
    @Override
    public Result save(GroupDto groupDto) {
        return null;
    }

    @Override
    public Result update(Long id, GroupDto groupDto) {
        return null;
    }

    @Override
    public Result softDelete(Long id) {
        return null;
    }

    @Override
    public Result delete(Long id) {
        return null;
    }
}
