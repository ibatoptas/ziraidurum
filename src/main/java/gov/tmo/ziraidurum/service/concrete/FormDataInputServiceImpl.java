package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Conctants.Messages;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessDataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.FormDataInputInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.FormDataInputShortInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPAdId;
import gov.tmo.ziraidurum.dto.abstacts.projections.UrunInfo;
import gov.tmo.ziraidurum.dto.concrete.FormDataInputDto;
import gov.tmo.ziraidurum.entities.concrete.FormDataInput;
import gov.tmo.ziraidurum.entities.concrete.Sube;
import gov.tmo.ziraidurum.entities.concrete.ZiraiDonem;
import gov.tmo.ziraidurum.repository.concrete.FormDataInputRepository;
import gov.tmo.ziraidurum.repository.concrete.SubeRepository;
import gov.tmo.ziraidurum.repository.concrete.ZiraiDonemRepository;
import gov.tmo.ziraidurum.service.abstacts.FormDataInputService;
import gov.tmo.ziraidurum.service.abstacts.mappers.FormDataInputMapper;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class FormDataInputServiceImpl implements FormDataInputService {

    private final FormDataInputRepository formDataInputRepository;
    private final SubeRepository subeRepository;
    private final ZiraiDonemRepository ziraiDonemRepository;
    private final FormDataInputMapper formDataInputMapper;

    public FormDataInputServiceImpl(FormDataInputRepository formDataInputRepository, SubeRepository subeRepository, ZiraiDonemRepository ziraiDonemRepository, FormDataInputMapper formDataInputMapper) {
        this.formDataInputRepository = formDataInputRepository;
        this.subeRepository = subeRepository;
        this.ziraiDonemRepository = ziraiDonemRepository;
        this.formDataInputMapper = formDataInputMapper;
    }

    @Override
    @Transactional
    public Result save(FormDataInputDto formDataInputDto) {
        formDataInputDto.setStatus(Boolean.TRUE);
//        formDataInputDto.setCreatedAt(new Date());
        FormDataInput formDataInput = formDataInputMapper.dtoToEntity(formDataInputDto);
        Sube sube = subeRepository.getById(formDataInputDto.getSubeId());
        formDataInput.setSube(sube);
        ZiraiDonem ziraiDonem = ziraiDonemRepository.getById(formDataInputDto.getZiraiDonemId());
        formDataInput.setZiraiDonem(ziraiDonem);
        this.formDataInputRepository.save(formDataInput);
        return new SuccessResult(Messages.formSaved);
    }

    @Override
    @Transactional
    public Result update(Long id, FormDataInputDto formDataInputDto) {
        Optional<FormDataInput> formIs = formDataInputRepository.findById(id);
        FormDataInput formDataInputDb = formIs.get();

        Sube sube = subeRepository.getById(formDataInputDto.getSubeId());
        formDataInputDb.setSube(sube);
        ZiraiDonem ziraiDonem = ziraiDonemRepository.getById(formDataInputDto.getZiraiDonemId());
        formDataInputDb.setZiraiDonem(ziraiDonem);

        formDataInputDb.setFormAciklama(formDataInputDto.getFormAciklama());
        formDataInputDb.setFormOnayStatus(formDataInputDto.getFormOnayStatus());
        formDataInputDb.setFormAciklama(formDataInputDto.getFormAciklama());

        this.formDataInputRepository.save(formDataInputDb);
        return new SuccessResult(Messages.formUpdated);
    }

    @Override
    public Result softDelete(Long id) {
        Optional<FormDataInput> formIs = formDataInputRepository.findById(id);
        FormDataInput formDataInputDb = formIs.get();

        formDataInputDb.setStatus(Boolean.FALSE);
        formDataInputRepository.save(formDataInputDb);

        return new SuccessResult(Messages.formDeleted);
    }

    @Override
    public Result delete(Long id) {
        this.formDataInputRepository.deleteById(id);
        return new SuccessResult(Messages.formDeleted);
    }

    @Override
    public DataResult<List<FormDataInputShortInfo>> getAllShort() {
        List<FormDataInputShortInfo> formDataInputShortInfos = formDataInputRepository.findProjectedByStatusTrueOrderById(FormDataInputShortInfo.class);
        return new SuccessDataResult<>(formDataInputShortInfos,Messages.formDataListed);
    }

    @Override
    public DataResult<List<FormDataInputInfo>> getAll() {
        List<FormDataInputInfo> formDataInputInfos = formDataInputRepository.findProjectedByStatusTrueOrderById(FormDataInputInfo.class);
        return new SuccessDataResult<>(formDataInputInfos,Messages.formDataListed);
    }

    @Override
    public DataResult<FormDataInputInfo> getById(Long id) {
        FormDataInputInfo formDataInputInfo = formDataInputRepository.findProjectedById(FormDataInputInfo.class,id);
        return new SuccessDataResult<>(formDataInputInfo,Messages.formGetted);
    }

    @Override
    public DataResult<FormDataInputShortInfo> getByIdShort(Long id) {
        FormDataInputShortInfo formDataInputInfo = formDataInputRepository.findProjectedById(FormDataInputShortInfo.class,id);
        return new SuccessDataResult<>(formDataInputInfo,Messages.formGetted);
    }
}
