package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Conctants.Messages;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessDataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPSubeShort;
import gov.tmo.ziraidurum.dto.concrete.SubeDto;
import gov.tmo.ziraidurum.entities.concrete.Bolge;
import gov.tmo.ziraidurum.entities.concrete.Sube;
import gov.tmo.ziraidurum.dto.abstacts.projections.SubeInfo;
import gov.tmo.ziraidurum.repository.concrete.BolgeRepository;
import gov.tmo.ziraidurum.repository.concrete.SubeRepository;
import gov.tmo.ziraidurum.service.abstacts.mappers.SubeMapper;
import gov.tmo.ziraidurum.service.abstacts.SubeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SubeServiceImpl implements SubeService {

    private final SubeRepository subeRepository;
    private final SubeMapper subeMapper;
    private final BolgeRepository bolgeRepository;

    public SubeServiceImpl(SubeRepository subeRepository, SubeMapper subeMapper, BolgeRepository bolgeRepository) {
        this.subeRepository = subeRepository;
        this.subeMapper = subeMapper;
        this.bolgeRepository = bolgeRepository;
    }


    @Override
    public Result save(SubeDto subeDto) {
        Sube sube = subeMapper.dtoToEntity(subeDto);
        Bolge bolge = bolgeRepository.getById(subeDto.getBolgeId());
        sube.setBolge(bolge);
        this.subeRepository.save(sube);
        return new SuccessResult(Messages.subeSaved);
    }

    @Override
    @Transactional
    public Result update(Long id, SubeDto subeDto) {
        Optional<Sube> subeIs = subeRepository.findById(id);
        Sube subeDb = subeIs.get();

        Bolge bolge = bolgeRepository.getById(subeDto.getBolgeId());

        subeDb.setSubeAdi(subeDto.getSubeAdi());
        subeDb.setSubeKodu(subeDto.getSubeKodu());
        subeDb.setBolge(bolge);
        this.subeRepository.save(subeDb);
        return new SuccessResult(Messages.subeUpdated);
    }

    @Override
    public Result softDelete(Long id) {
        Optional<Sube> subeIs = subeRepository.findById(id);
        Sube subeDb = subeIs.get();

        subeDb.setStatus(Boolean.FALSE);
        subeRepository.save(subeDb);

        return new SuccessResult(Messages.subeDeleted);
    }

    @Override
    public Result delete(Long id) {
        this.subeRepository.deleteById(id);
        return new SuccessResult(Messages.subeDeleted);
    }

    @Override
    public DataResult<List<SubeInfo>> getAll() {
        List<SubeInfo> subeDatas = subeRepository.findProjectedBy(SubeInfo.class);
        return new SuccessDataResult<>(subeDatas,Messages.subeListed);
    }

    @Override
    public DataResult<SubeInfo> getById(Long id) {
        SubeInfo  subeInfo = subeRepository.findById(SubeInfo.class,id);
        return new SuccessDataResult<>(subeInfo,Messages.subeGetted);
    }

    @Override
    public DataResult<List<IPSubeShort>> getSubeShortList() {
        List<IPSubeShort> result = subeRepository.findProjectedBy(IPSubeShort.class);
        return new SuccessDataResult<>(result);
    }
}
