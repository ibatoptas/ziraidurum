package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.concrete.DataInputDto;
import gov.tmo.ziraidurum.service.abstacts.DataInputService;
import org.springframework.stereotype.Service;

@Service
public class DataInputServiceImpl implements DataInputService {
    @Override
    public Result save(DataInputDto dataInputDto) {
        return null;
    }

    @Override
    public Result update(Long id, DataInputDto dataInputDto) {
        return null;
    }

    @Override
    public Result softDelete(Long id) {
        return null;
    }

    @Override
    public Result delete(Long id) {
        return null;
    }
}
