package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.dto.concrete.ZiraiDonemSummaryDto;
import gov.tmo.ziraidurum.entities.concrete.Sube;
import gov.tmo.ziraidurum.entities.concrete.ZiraiDonem;
import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;
import gov.tmo.ziraidurum.service.abstacts.QueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.Date;
import java.util.List;

/**
 * Created By ibrahim.toptas
 * Created At 14.10.2021 - 14:18
 **/
@Component
@RequiredArgsConstructor
public class QueryServiceImpl implements QueryService {
    private final EntityManager em;

    @Override
    public List<ZiraiDonemSummaryDto> getZiraiDonemSummary(Long ekimDonemId, Long urunId) {
        CriteriaBuilder b = em.getCriteriaBuilder();
        CriteriaQuery<ZiraiDonemSummaryDto> q = b.createQuery(ZiraiDonemSummaryDto.class);
        Root<ZiraiDonem> r = q.from(ZiraiDonem.class);
        Join<Object, Object> jekimDonemi = r.join("ekimDonemi");
        Join<Object, Object> jurun = r.join("urun");

        Subquery<Long> subSube = q.subquery(Long.class);
        Root<Sube> rSube = subSube.from(Sube.class);
        subSube.select(b.count(rSube)).where(b.equal(rSube.get("status"), true));

        Subquery<Long> girisYapanQuery = q.subquery(Long.class);
        Root<ZiraiDonem> rGirisYapan = girisYapanQuery.correlate(r);
        Join<Object, Object> jformDataInput = rGirisYapan.join("formDataInputs", JoinType.LEFT);
        girisYapanQuery.select(b.count(jformDataInput));

        Subquery<Long> onaylanmamisQuery = q.subquery(Long.class);
        Root<ZiraiDonem> ronaylanmamis = onaylanmamisQuery.correlate(r);
        Join<Object, Object> jonayForm = ronaylanmamis.join("formDataInputs", JoinType.LEFT);
        onaylanmamisQuery.select(b.count(jonayForm))
                .where(b.notEqual(jonayForm.get("formOnayStatus"), EFormOnayStatus.MERKEZ));


        Date date = new Date();

        q.select(b.construct(ZiraiDonemSummaryDto.class,
                        r.get("id"),
                        r.get("donemAdi"),
                        r.get("baslangicTarihi"),
                        r.get("bitisTarihi"),
                        r.get("aciklama"),
                        b.selectCase()
                                .when(b.between(b.literal(date), r.get("baslangicTarihi"), r.get("bitisTarihi")), 0)
                                .when(b.greaterThan(r.get("bitisTarihi"), date), 1)
                                .otherwise(2),
                        b.diff(subSube, girisYapanQuery),
                        onaylanmamisQuery
                ))
                .where(
                        b.equal(jekimDonemi.get("id"), ekimDonemId),
                        b.equal(jurun.get("id"), urunId)
                )
                .orderBy(
                        b.desc(r.get("bitisTarihi"))
                );
        return em.createQuery(q).getResultList();
    }
}
