package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Conctants.Messages;
import gov.tmo.ziraidurum.core.Utilities.Results.*;
import gov.tmo.ziraidurum.dto.abstacts.projections.EkimDonemiInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPEkimDonemiShort;
import gov.tmo.ziraidurum.dto.concrete.EkimDonemiDto;
import gov.tmo.ziraidurum.entities.concrete.*;
import gov.tmo.ziraidurum.repository.concrete.EkimDonemiRepository;
import gov.tmo.ziraidurum.repository.concrete.UrunRepository;
import gov.tmo.ziraidurum.repository.concrete.YearRepository;
import gov.tmo.ziraidurum.service.abstacts.EkimDonemiService;
import gov.tmo.ziraidurum.service.abstacts.mappers.EkimDonemiMapper;
import gov.tmo.ziraidurum.service.abstacts.mappers.UrunMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EkimDonemiServiceImpl implements EkimDonemiService {

    private final EkimDonemiRepository ekimDonemiRepository;
    private final EkimDonemiMapper ekimDonemiMapper;
    private final UrunRepository urunRepository;
    private final UrunMapper urunMapper;
    private final YearRepository yearRepository;

    public EkimDonemiServiceImpl(EkimDonemiRepository ekimDonemiRepository,  EkimDonemiMapper ekimDonemiMapper,UrunRepository urunRepository, UrunMapper urunMapper, YearRepository yearRepository) {
        this.ekimDonemiRepository = ekimDonemiRepository;
        this.ekimDonemiMapper = ekimDonemiMapper;
        this.urunRepository = urunRepository;
        this.urunMapper = urunMapper;
        this.yearRepository = yearRepository;
    }


    @Override
    @Transactional
    public Result save(EkimDonemiDto ekimDonemiDto) {
//        ekimDonemiDto.setCreatedAt(new Date());
        ekimDonemiDto.setStatus(Boolean.TRUE);
        Year year = yearRepository.getById(ekimDonemiDto.getYearId());
        ekimDonemiDto.setYear(year);
        EkimDonemi ekimDonemi = ekimDonemiMapper.dtoToEntity(ekimDonemiDto);
        this.ekimDonemiRepository.save(ekimDonemi);
        return new SuccessResult(Messages.ekimDonemiSaved);
    }

    @Override
    @Transactional
    public Result update(Long id, EkimDonemiDto ekimDonemiDto) {
        Optional<EkimDonemi> ekimDonemiIs = ekimDonemiRepository.findById(id);
        EkimDonemi ekimDonemiDb = ekimDonemiIs.get();
        Year year = yearRepository.getById(ekimDonemiDto.getYearId());
        ekimDonemiDb.setEkimAdi(ekimDonemiDto.getEkimAdi());
        ekimDonemiDb.setAciklama(ekimDonemiDto.getAciklama());
        ekimDonemiDb.setYear(year);
        ekimDonemiDb.setUpdatedAt(new Date());
        this.ekimDonemiRepository.save(ekimDonemiDb);
        return new SuccessResult(Messages.ekimDonemiUpdated);
    }

    @Override
    public Result softDelete(Long id) {
        Optional<EkimDonemi> ekimDonemiIs = ekimDonemiRepository.findById(id);
        EkimDonemi ekimDonemiDb = ekimDonemiIs.get();
        ekimDonemiDb.setStatus(Boolean.FALSE);
        ekimDonemiRepository.save(ekimDonemiDb);
        return new SuccessResult(Messages.ekimDonemiDeleted);
    }

    @Override
    public Result delete(Long id) {
        this.ekimDonemiRepository.deleteById(id);
        return new SuccessResult(Messages.ekimDonemiDeleted);
    }


    @Override
    public DataResult<List<EkimDonemiInfo>> getAll() {
        List<EkimDonemiInfo> ekimDonemiInfos = ekimDonemiRepository.findProjectedByStatusTrueOrderById(EkimDonemiInfo.class);
        return new SuccessDataResult<>(ekimDonemiInfos, Messages.ekimDonemiListed);
    }

    @Override
    public DataResult<EkimDonemiInfo> getById(Long id) {
        EkimDonemiInfo ekimDonemiInfo = ekimDonemiRepository.findProjectedById(EkimDonemiInfo.class, id);
        return new SuccessDataResult<>(ekimDonemiInfo, Messages.ekimDonemiGetted);
    }

    @Override
    public DataResult<List<IPEkimDonemiShort>> getShortList() {
        List<IPEkimDonemiShort> result = ekimDonemiRepository.findProjectedByStatusTrueOrderById(IPEkimDonemiShort.class);
        return new SuccessDataResult<>(result);
    }


}
