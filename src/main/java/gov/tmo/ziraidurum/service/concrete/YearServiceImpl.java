package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Conctants.Messages;
import gov.tmo.ziraidurum.core.Utilities.Results.*;
import gov.tmo.ziraidurum.dto.abstacts.projections.YearInfo;
import gov.tmo.ziraidurum.dto.concrete.YearDto;
import gov.tmo.ziraidurum.entities.concrete.Year;
import gov.tmo.ziraidurum.repository.concrete.YearRepository;
import gov.tmo.ziraidurum.service.abstacts.YearService;
import gov.tmo.ziraidurum.service.abstacts.mappers.YearMapper;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class YearServiceImpl implements YearService {

    private final YearRepository yearRepository;
    private final YearMapper yearMapper;

    public YearServiceImpl(YearRepository yearRepository, YearMapper yearMapper) {
        this.yearRepository = yearRepository;
        this.yearMapper = yearMapper;
    }

    @Override
    public Result save(YearDto yearDto) {
        Year yearDb = yearRepository.findByYearName(yearDto.getYearName());
        if(yearDb != null)
        {
            return new ErrorResult(Messages.yearNameError);
        }

        yearDto.setStatus(Boolean.TRUE);
//        yearDto.setCreatedAt(new Date());
        Year year = yearMapper.dtoToEntity(yearDto);
        this.yearRepository.save(year);
        return new SuccessResult(Messages.yearSaved);

    }

    @Override
    public Result update(Long id, YearDto yearDto) {
        Optional<Year> yearIs = yearRepository.findById(id);
        Year yearDb = yearIs.get();

        Year yearCheck = yearRepository.findByYearNameAndIdIsNot(yearDto.getYearName(),id);
        if(yearCheck != null){
            return new ErrorResult(Messages.yearNameError);
        }

        yearDb.setYearName(yearDto.getYearName());

        return new SuccessResult(Messages.yearUpdated);
    }

    @Override
    public Result softDelete(Long id) {
        Optional<Year> yearIs = yearRepository.findById(id);
        Year yearDb = yearIs.get();

        yearDb.setStatus(Boolean.FALSE);
        yearRepository.save(yearDb);

        return new SuccessResult(Messages.yearDeleted);
    }

    @Override
    public Result delete(Long id) {
        this.yearRepository.deleteById(id);
        return new SuccessResult(Messages.yearDeleted);
    }

    @Override
    public DataResult<List<YearInfo>> getAll() {
        List<YearInfo> yearInfos = yearRepository.findProjectedByStatusTrueOrderByYearName(YearInfo.class);
        return new SuccessDataResult<>(yearInfos,Messages.yearListed);
    }

    @Override
    public DataResult<YearInfo> getById(Long id) {
        YearInfo  yearInfo = yearRepository.findProjectedById(YearInfo.class,id);
        return new SuccessDataResult<>(yearInfo,Messages.yearGetted);
    }
}
