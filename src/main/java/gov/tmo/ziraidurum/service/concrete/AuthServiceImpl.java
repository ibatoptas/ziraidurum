package gov.tmo.ziraidurum.service.concrete;

import com.fasterxml.jackson.core.JsonProcessingException;
import gov.tmo.ziraidurum.config.security.JwtUtil;
import gov.tmo.ziraidurum.config.security.LoggedUserModel;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.concrete.LoginDto;
import gov.tmo.ziraidurum.service.abstacts.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created By ibrahim.toptas
 * Created At 20.10.2021 - 11:11
 **/
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;

    @Override
    public Result login(LoginDto loginDto) throws JsonProcessingException {
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
        String token = jwtUtil.generateToken(authenticate);
        LoggedUserModel loggedUserModel = (LoggedUserModel) authenticate.getPrincipal();
        Map<String, Object> result = new HashMap<>();
        result.put("token", token);
        result.put("info", loggedUserModel);
        return new DataResult<>(result, true);
    }
}
