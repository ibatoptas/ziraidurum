package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Conctants.Messages;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessDataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.OnayInfo;
import gov.tmo.ziraidurum.dto.concrete.OnayDto;
import gov.tmo.ziraidurum.entities.concrete.FormDataInput;
import gov.tmo.ziraidurum.entities.concrete.Onay;
import gov.tmo.ziraidurum.entities.concrete.User;
import gov.tmo.ziraidurum.repository.concrete.FormDataInputRepository;
import gov.tmo.ziraidurum.repository.concrete.OnayRepository;
import gov.tmo.ziraidurum.repository.concrete.UserRepository;
import gov.tmo.ziraidurum.service.abstacts.OnayService;
import gov.tmo.ziraidurum.service.abstacts.mappers.OnayMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OnayServiceImpl implements OnayService {

    private final OnayRepository onayRepository;
    private final OnayMapper onayMapper;
    private final UserRepository userRepository;
    private final FormDataInputRepository formDataInputRepository;

    public OnayServiceImpl(OnayRepository onayRepository, OnayMapper onayMapper, UserRepository userRepository, FormDataInputRepository formDataInputRepository) {
        this.onayRepository = onayRepository;
        this.onayMapper = onayMapper;
        this.userRepository = userRepository;
        this.formDataInputRepository = formDataInputRepository;
    }

    @Override
    @Transactional
    public Result save(OnayDto onayDto) {
      Onay onay = onayMapper.dtoToEntity(onayDto);
      FormDataInput formDataInput = formDataInputRepository.getById(onayDto.getFormDataInputId());
      User user = userRepository.getById(onayDto.getUserId());
      onay.setFormDataInput(formDataInput);
      onay.setUser(user);
      onay.setStatus(Boolean.TRUE);
      onay.setCreatedAt(new Date());
      onay.setSignature(UUID.randomUUID());
      this.onayRepository.save(onay);
      return new SuccessResult(Messages.onaySaved);
    }

    @Override
    @Transactional
    public Result update(Long id, OnayDto onayDto) {
        Optional<Onay> onayIs = onayRepository.findById(onayDto.getId());
        Onay onayDb = onayIs.get();

        FormDataInput formDataInput = formDataInputRepository.getById(onayDto.getFormDataInputId());
        User user = userRepository.getById(onayDto.getUserId());

        onayDb.setUser(user);
        onayDb.setOnayDurum(onayDto.getOnayDurum());
        onayDb.setUpdatedAt(new Date());
        onayDb.setFormDataInput(formDataInput);
        onayDb.setAciklama(onayDto.getAciklama());
        onayDb.setSignature(UUID.randomUUID());
        return new SuccessResult(Messages.onayUpdated);
    }

    @Override
    public Result softDelete(Long id) {
        Optional<Onay> onayIs = onayRepository.findById(id);
        Onay onayDb = onayIs.get();
        onayDb.setStatus(Boolean.FALSE);
        onayRepository.save(onayDb);
        return new SuccessResult(Messages.onayDeleted);
    }

    @Override
    public Result delete(Long id) {
        this.onayRepository.deleteById(id);
        return new SuccessResult(Messages.onayDeleted);
    }

    @Override
    public DataResult<List<OnayInfo>> getAll() {
        List<OnayInfo> onayInfos = onayRepository.findProjectedByStatusTrue(OnayInfo.class);
        return new SuccessDataResult<>(onayInfos,Messages.onayDataListed);
    }

    @Override
    public DataResult<OnayInfo> getById(Long id) {
        OnayInfo onayInfo = onayRepository.findProjectedByIdAndStatusTrue(OnayInfo.class,id);
        return new SuccessDataResult<>(onayInfo,Messages.onayGetted);
    }
}
