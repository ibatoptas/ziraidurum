package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Conctants.Messages;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessDataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.BolgeInfo;
import gov.tmo.ziraidurum.dto.concrete.BolgeDto;
import gov.tmo.ziraidurum.entities.concrete.Bolge;
import gov.tmo.ziraidurum.repository.concrete.BolgeRepository;
import gov.tmo.ziraidurum.service.abstacts.mappers.BolgeMapper;
import gov.tmo.ziraidurum.service.abstacts.BolgeService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BolgeServiceImpl implements BolgeService {

    private final BolgeRepository bolgeRepository;
    private final BolgeMapper bolgeMapper;

    public BolgeServiceImpl(BolgeRepository bolgeRepository, BolgeMapper bolgeMapper) {
        this.bolgeRepository = bolgeRepository;
        this.bolgeMapper = bolgeMapper;
    }


    @Override
    public Result save(BolgeDto bolgeDto) {
        Bolge bolge = bolgeMapper.dtoToEntity(bolgeDto);
        bolge.setStatus(Boolean.TRUE);
        bolge.setCreatedAt(new Date());
        this.bolgeRepository.save(bolge);
        return new SuccessResult(Messages.bolgeSaved);
    }

    @Override
    public Result update(Long id, BolgeDto bolgeDto) {
        Optional<Bolge> bolgeIs = bolgeRepository.findById(id);
        Bolge bolgeDb = bolgeIs.get();

        bolgeDb.setName(bolgeDto.getName());
        bolgeDb.setUpdatedAt(new Date());
        this.bolgeRepository.save(bolgeDb);
        return new SuccessResult(Messages.bolgeUpdated);
    }

    @Override
    public Result softDelete(Long id) {
        this.bolgeRepository.deleteById(id);
        return new SuccessResult(Messages.bolgeDeleted);
    }

    @Override
    public Result delete(Long id) {
        this.bolgeRepository.deleteById(id);
        return new SuccessResult(Messages.bolgeDeleted);
    }

    @Override
    public DataResult<List<BolgeInfo>> getAll() {
        List<BolgeInfo> bolgeInfos = bolgeRepository.findProjectedBy(BolgeInfo.class);
        return new SuccessDataResult<>(bolgeInfos,Messages.bolgeListed);
    }

    @Override
    public DataResult<BolgeInfo> getById(Long id) {
        BolgeInfo  bolgeInfo = bolgeRepository.findProjectedById(BolgeInfo.class,id);
        return new SuccessDataResult<>(bolgeInfo,Messages.bolgeGetted);
    }
}
