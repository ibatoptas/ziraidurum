package gov.tmo.ziraidurum.service.concrete;

import gov.tmo.ziraidurum.core.Utilities.Conctants.Messages;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessDataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.SuccessResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPZiraiDonemBase;
import gov.tmo.ziraidurum.dto.abstacts.projections.ZiraiDonemInfo;
import gov.tmo.ziraidurum.dto.concrete.ZiraiDonemDto;
import gov.tmo.ziraidurum.dto.concrete.ZiraiDonemSummaryDto;
import gov.tmo.ziraidurum.entities.concrete.EkimDonemi;
import gov.tmo.ziraidurum.entities.concrete.Urun;
import gov.tmo.ziraidurum.entities.concrete.ZiraiDonem;
import gov.tmo.ziraidurum.repository.concrete.EkimDonemiRepository;
import gov.tmo.ziraidurum.repository.concrete.UrunRepository;
import gov.tmo.ziraidurum.repository.concrete.ZiraiDonemRepository;
import gov.tmo.ziraidurum.service.abstacts.QueryService;
import gov.tmo.ziraidurum.service.abstacts.ZiraiDonemService;
import gov.tmo.ziraidurum.service.abstacts.mappers.ZiraiDonemMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ZiraiDonemServiceImpl implements ZiraiDonemService {

    private final ZiraiDonemRepository ziraiDonemRepository;
    private final ZiraiDonemMapper ziraiDonemMapper;
    private final EkimDonemiRepository ekimDonemiRepository;
    private final UrunRepository urunRepository;
    private final QueryService queryService;

    public ZiraiDonemServiceImpl(ZiraiDonemRepository ziraiDonemRepository, EkimDonemiRepository ekimDonemiRepository, ZiraiDonemMapper ziraiDonemMapper, EkimDonemiRepository ekimDonemiRepository1, UrunRepository urunRepository, QueryService queryService) {
        this.ziraiDonemRepository = ziraiDonemRepository;
        this.ziraiDonemMapper = ziraiDonemMapper;
        this.ekimDonemiRepository = ekimDonemiRepository1;
        this.urunRepository = urunRepository;
        this.queryService = queryService;
    }

    @Override
    @Transactional
    public Result save(ZiraiDonemDto ziraiDonemDto) {
//        ziraiDonemDto.setCreatedAt(new Date());
        ziraiDonemDto.setStatus(Boolean.TRUE);
        ZiraiDonem ziraiDonem = ziraiDonemMapper.dtoToEntity(ziraiDonemDto);
        EkimDonemi ekimDonemi = ekimDonemiRepository.getById(ziraiDonemDto.getEkimDonemId());
        Urun urun = urunRepository.getById(ziraiDonemDto.getUrunId());
        ziraiDonem.setUrun(urun);
        ziraiDonem.setEkimDonemi(ekimDonemi);
        this.ziraiDonemRepository.save(ziraiDonem);
        return new SuccessResult(Messages.ziraiDonemSaved);
    }

    @Override
    @Transactional
    public Result update(Long id, ZiraiDonemDto ziraiDonemDto) {
//        ziraiDonemDto.setUpdatedAt(new Date());
        Optional<ZiraiDonem> ziraiDonemIs = ziraiDonemRepository.findById(id);
        ZiraiDonem ziraiDonemDb = ziraiDonemIs.get();
        EkimDonemi ekimDonemi = ekimDonemiRepository.getById(ziraiDonemDto.getEkimDonemId());
        Urun urun = urunRepository.getById(ziraiDonemDto.getUrunId());
        ziraiDonemDb.setDonemAdi(ziraiDonemDto.getDonemAdi());
        ziraiDonemDb.setAciklama(ziraiDonemDto.getAciklama());
        ziraiDonemDb.setAciklama(ziraiDonemDto.getAciklama());
        ziraiDonemDb.setBaslangicTarihi(ziraiDonemDto.getBaslangicTarihi());
        ziraiDonemDb.setBitisTarihi(ziraiDonemDto.getBitisTarihi());
        ziraiDonemDb.setUrun(urun);
        ziraiDonemDb.setEkimDonemi(ekimDonemi);
        this.ziraiDonemRepository.save(ziraiDonemDb);
        return new SuccessResult(Messages.ziraiDonemUpdated);
    }

    @Override
    public Result softDelete(Long id) {
        Optional<ZiraiDonem> ziraiDonemIs = ziraiDonemRepository.findById(id);
        ZiraiDonem ziraiDonemDb = ziraiDonemIs.get();

        ziraiDonemDb.setStatus(Boolean.FALSE);
        ziraiDonemRepository.save(ziraiDonemDb);

        return new SuccessResult(Messages.ziraiDonemDeleted);
    }

    @Override
    public Result delete(Long id) {
        this.ziraiDonemRepository.deleteById(id);
        return new SuccessResult(Messages.ziraiDonemDeleted);
    }

    @Override
    public DataResult<List<ZiraiDonemInfo>> getAll() {
        List<ZiraiDonemInfo> ziraiDonemInfos = ziraiDonemRepository.findProjectedByStatusTrueOrderByEkimDonemiId(ZiraiDonemInfo.class);
        return new SuccessDataResult<>(ziraiDonemInfos,Messages.ziraiDonemListed);
    }

    @Override
    public DataResult<ZiraiDonemInfo> getById(Long id) {
        ZiraiDonemInfo ziraiDonemInfo = ziraiDonemRepository.findProjectedById(ZiraiDonemInfo.class,id);
        return new SuccessDataResult<>(ziraiDonemInfo,Messages.ziraiDonemGetted);
    }

    @Override
    public DataResult<List<ZiraiDonemSummaryDto>> getSummary(Long ekimDonemId, Long urunId) {
        List<ZiraiDonemSummaryDto> result = queryService.getZiraiDonemSummary(ekimDonemId, urunId);
        return new SuccessDataResult<>(result);
    }

    @Override
    public DataResult<List<IPZiraiDonemBase>> getZiraiDonemsByDonemAndUrunForTasra(Long ekimDonemId, Long urunId) {
        List<IPZiraiDonemBase> result = ziraiDonemRepository
                .findByEkimDonemi_IdAndUrun_IdAndBaslangicTarihiLessThanEqualOrderByBitisTarihiDesc(IPZiraiDonemBase.class, ekimDonemId, urunId,new Date());
        return new SuccessDataResult<>(result);
    }
}
