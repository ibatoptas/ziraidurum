package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.FormDataInputDto;
import gov.tmo.ziraidurum.dto.concrete.FormModelDto;
import gov.tmo.ziraidurum.entities.concrete.FormDataInput;
import gov.tmo.ziraidurum.entities.concrete.FormModel;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FormDataInputMapper {

    FormDataInput dtoToEntity(FormDataInputDto formDataInputDto);

    FormDataInputDto entityToDto(FormDataInput formDataInput);

    List<FormDataInputDto> entityListToDtoList(List<FormDataInput> formDataInputList);

    List<FormDataInput> dtoListToEntityList(List<FormDataInputDto> formDataInputDtoList);
}
