package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.dto.concrete.CaptionDto;

public interface CaptionService extends BaseService<CaptionDto>{
}
