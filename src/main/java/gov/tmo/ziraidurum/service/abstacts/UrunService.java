package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPAdId;
import gov.tmo.ziraidurum.dto.abstacts.projections.UrunInfo;
import gov.tmo.ziraidurum.dto.concrete.UrunDto;

import java.util.List;

public interface UrunService extends BaseService<UrunDto> {

    DataResult<List<UrunInfo>> getAll();

    DataResult<List<IPAdId>> getUrunList();

    DataResult<UrunInfo> getById(Long id);

}
