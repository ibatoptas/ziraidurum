package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.UrunDto;
import gov.tmo.ziraidurum.dto.concrete.YearDto;
import gov.tmo.ziraidurum.entities.concrete.Urun;
import gov.tmo.ziraidurum.entities.concrete.Year;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface YearMapper {

    Year dtoToEntity(YearDto yearDto);

    YearDto entityToDto(Year year);

    List<YearDto> entityListToDtoList(List<Year> yearList);

    List<Year> dtoListToEntityList(List<YearDto> yearDtoList);
}
