package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.BolgeDto;
import gov.tmo.ziraidurum.dto.concrete.CaptionDto;
import gov.tmo.ziraidurum.entities.concrete.Bolge;
import gov.tmo.ziraidurum.entities.concrete.Caption;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CaptionMapper {

    Caption dtoToEntity(CaptionDto captionDto);

    CaptionDto entityToDto(Caption caption);

    List<CaptionDto> entityListToDtoList(List<Caption> captionList);

    List<Caption> dtoListToEntityList(List<CaptionDto> captionDtoList);
}
