package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPSubeShort;
import gov.tmo.ziraidurum.dto.abstacts.projections.SubeInfo;
import gov.tmo.ziraidurum.dto.concrete.SubeDto;

import java.util.List;

public interface SubeService extends BaseService<SubeDto>{

    DataResult<List<SubeInfo>> getAll();

    DataResult<SubeInfo> getById(Long id);

    DataResult<List<IPSubeShort>> getSubeShortList();
}
