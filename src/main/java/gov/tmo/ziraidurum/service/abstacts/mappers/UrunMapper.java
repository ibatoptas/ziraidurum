package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.UrunDto;
import gov.tmo.ziraidurum.entities.concrete.Urun;
import org.mapstruct.Mapper;
import org.springframework.context.annotation.Bean;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UrunMapper {

    Urun dtoToEntity(UrunDto urunDto);

    UrunDto entityToDto(Urun urun);

    List<UrunDto> entityListToDtoList(List<Urun> urunList);

    List<Urun> dtoListToEntityList(List<UrunDto> personDTOList);

}
