package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.concrete.FormModelDto;
import gov.tmo.ziraidurum.dto.concrete.TasraFormSaveDto;
import gov.tmo.ziraidurum.dto.concrete.UrunBaslikSaveDto;

public interface FormModelService extends BaseService<FormModelDto>{

    Result saveFormModel(UrunBaslikSaveDto urunBaslikSaveDto);

    Result getFormDataInputs(Long ziraiDonemId);

    Result saveFormDatas(TasraFormSaveDto dto);

    Result getFormDataInputsBySube(Long ziraiDonemId, Long subeId);

    Result getFormDataInputsByUrunAndSube(Long donemId, Long urunId, Long subeId);

    Result setFormDataInputOnay(Boolean act, Long formId);

    Result updateFormDataInputOnay(Boolean act, Long formDataInputId);
}
