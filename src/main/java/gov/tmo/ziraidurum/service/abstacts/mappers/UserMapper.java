package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.UrunDto;
import gov.tmo.ziraidurum.dto.concrete.UserDto;
import gov.tmo.ziraidurum.entities.concrete.Urun;
import gov.tmo.ziraidurum.entities.concrete.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User dtoToEntity(UserDto userDto);

    UserDto entityToDto(User user);

    List<UserDto> entityListToDtoList(List<User> userList);

    List<User> dtoListToEntityList(List<UserDto> userDtoList);
}
