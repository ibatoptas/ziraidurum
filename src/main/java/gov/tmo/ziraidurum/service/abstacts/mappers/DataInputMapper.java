package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.CaptionDto;
import gov.tmo.ziraidurum.dto.concrete.DataInputDto;
import gov.tmo.ziraidurum.entities.concrete.Caption;
import gov.tmo.ziraidurum.entities.concrete.DataInput;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DataInputMapper {

    DataInput dtoToEntity(DataInputDto dataInputDto);

    DataInputDto entityToDto(DataInput dataInput);

    List<DataInputDto> entityListToDtoList(List<DataInput> dataInputList);

    List<DataInput> dtoListToEntityList(List<DataInputDto> dataInputDtoList);
}
