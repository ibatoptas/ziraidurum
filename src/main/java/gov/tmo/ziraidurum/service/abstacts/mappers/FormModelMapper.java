package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.FormModelDto;
import gov.tmo.ziraidurum.dto.concrete.UrunDto;
import gov.tmo.ziraidurum.entities.concrete.FormModel;
import gov.tmo.ziraidurum.entities.concrete.Urun;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FormModelMapper {

    FormModel dtoToEntity(FormModelDto formModelDto);

    FormModelDto entityToDto(FormModel formModel);

    List<FormModelDto> entityListToDtoList(List<FormModel> formModelList);

    List<FormModel> dtoListToEntityList(List<FormModelDto> formModelDtoList);
}
