package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.GroupDto;
import gov.tmo.ziraidurum.dto.concrete.SubeDto;
import gov.tmo.ziraidurum.entities.concrete.Groups;
import gov.tmo.ziraidurum.entities.concrete.Sube;
import org.mapstruct.Mapper;

import javax.swing.*;
import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupMapper {

    Groups dtoToEntity(GroupDto groupDto);

    GroupDto entityToDto(Groups groups);

    List<GroupDto> entityListToDtoList(List<Groups> groupsList);

    List<Groups> dtoListToEntityList(List<GroupDto> groupDtoList);
}
