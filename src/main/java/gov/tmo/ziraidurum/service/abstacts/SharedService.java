package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.config.security.LoggedUserModel;
import gov.tmo.ziraidurum.entities.concrete.enums.ERoleType;

/**
 * Created by iBa at 15.10.2021 23:26
 */
public interface SharedService {
    Long getSubeId();

    ERoleType getUserRole();

    Integer getUserOnayLevel();

    LoggedUserModel getLoggedUser();
}
