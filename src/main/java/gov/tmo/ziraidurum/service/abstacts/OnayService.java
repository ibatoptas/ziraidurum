package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.OnayInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.SubeInfo;
import gov.tmo.ziraidurum.dto.concrete.OnayDto;

import java.util.List;

public interface OnayService extends BaseService<OnayDto>{

    DataResult<List<OnayInfo>> getAll();

    DataResult<OnayInfo> getById(Long id);
}
