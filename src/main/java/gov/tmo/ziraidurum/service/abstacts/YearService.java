package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.YearInfo;
import gov.tmo.ziraidurum.dto.concrete.YearDto;

import java.util.List;

public interface YearService extends BaseService<YearDto>{

    DataResult<List<YearInfo>> getAll();

    DataResult<YearInfo> getById(Long id);
}
