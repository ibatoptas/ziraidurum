package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.BolgeDto;
import gov.tmo.ziraidurum.dto.concrete.UrunDto;
import gov.tmo.ziraidurum.entities.concrete.Bolge;
import gov.tmo.ziraidurum.entities.concrete.Urun;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BolgeMapper {

    Bolge dtoToEntity(BolgeDto bolgeDto);

    BolgeDto entityToDto(Bolge bolge);

    List<BolgeDto> entityListToDtoList(List<Bolge> bolgeList);

    List<Bolge> dtoListToEntityList(List<BolgeDto> bolgeDtoList);

}
