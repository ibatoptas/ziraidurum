package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.BolgeInfo;
import gov.tmo.ziraidurum.dto.concrete.BolgeDto;

import java.util.List;

public interface BolgeService extends BaseService<BolgeDto>{

    DataResult<List<BolgeInfo>> getAll();

    DataResult<BolgeInfo> getById(Long id);
}
