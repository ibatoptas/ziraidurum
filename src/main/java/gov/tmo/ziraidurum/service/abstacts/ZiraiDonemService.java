package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPZiraiDonemBase;
import gov.tmo.ziraidurum.dto.abstacts.projections.ZiraiDonemInfo;
import gov.tmo.ziraidurum.dto.concrete.ZiraiDonemDto;
import gov.tmo.ziraidurum.dto.concrete.ZiraiDonemSummaryDto;

import java.util.List;

public interface ZiraiDonemService extends BaseService<ZiraiDonemDto>{

    DataResult<List<ZiraiDonemInfo>> getAll();

    DataResult<ZiraiDonemInfo> getById(Long id);

    DataResult<List<ZiraiDonemSummaryDto>> getSummary(Long ekimDonemId, Long urunId);

    DataResult<List<IPZiraiDonemBase>> getZiraiDonemsByDonemAndUrunForTasra(Long ekimDonemId, Long urunId);
}
