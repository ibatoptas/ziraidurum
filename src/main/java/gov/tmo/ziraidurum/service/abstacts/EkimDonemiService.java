package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.EkimDonemiInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPEkimDonemiShort;
import gov.tmo.ziraidurum.dto.concrete.EkimDonemiDto;

import java.util.List;

public interface EkimDonemiService extends BaseService<EkimDonemiDto>{

    DataResult<List<EkimDonemiInfo>> getAll();

    DataResult<EkimDonemiInfo> getById(Long id);

    DataResult<List<IPEkimDonemiShort>> getShortList();
}
