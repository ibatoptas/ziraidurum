package gov.tmo.ziraidurum.service.abstacts;

import com.fasterxml.jackson.core.JsonProcessingException;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.concrete.LoginDto;

/**
 * Created By ibrahim.toptas
 * Created At 20.10.2021 - 11:11
 **/
public interface AuthService {
    Result login(LoginDto loginDto) throws JsonProcessingException;
}
