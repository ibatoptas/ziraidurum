package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.BaslikInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPBaslik;
import gov.tmo.ziraidurum.dto.abstacts.projections.UrunInfo;
import gov.tmo.ziraidurum.dto.concrete.BaslikDto;

import java.util.List;

public interface BaslikService extends BaseService<BaslikDto>{

    DataResult<List<IPBaslik>> getAll();

    DataResult<List<IPBaslik>> getBasliksByUrunId(Long urunId);
}
