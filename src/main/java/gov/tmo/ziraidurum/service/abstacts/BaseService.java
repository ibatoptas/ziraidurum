package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.entities.abstracts.IEntity;

import java.util.List;

public interface BaseService<T extends IDto>  {

    Result save(T t);

    Result update(Long id, T t);

    Result softDelete(Long id);

    Result delete(Long id);

}
