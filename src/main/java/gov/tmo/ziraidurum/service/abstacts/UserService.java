package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.dto.concrete.UserDto;

public interface UserService extends BaseService<UserDto>{
}
