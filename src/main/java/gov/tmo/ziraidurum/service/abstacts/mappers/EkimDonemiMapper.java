package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.BolgeDto;
import gov.tmo.ziraidurum.dto.concrete.EkimDonemiDto;
import gov.tmo.ziraidurum.entities.concrete.Bolge;
import gov.tmo.ziraidurum.entities.concrete.EkimDonemi;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EkimDonemiMapper {

    EkimDonemi dtoToEntity(EkimDonemiDto ekimDonemiDto);

    EkimDonemiDto entityToDto(EkimDonemi ekimDonemi);

    List<EkimDonemiDto> entityListToDtoList(List<EkimDonemi> ekimDonemiList);

    List<EkimDonemi> dtoListToEntityList(List<EkimDonemiDto> ekimDonemiDtoList);
}
