package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.abstacts.IDto;
import gov.tmo.ziraidurum.dto.concrete.EkimDonemiDto;
import gov.tmo.ziraidurum.dto.concrete.ZiraiDonemDto;
import gov.tmo.ziraidurum.entities.concrete.EkimDonemi;
import gov.tmo.ziraidurum.entities.concrete.ZiraiDonem;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ZiraiDonemMapper{

    ZiraiDonem dtoToEntity(ZiraiDonemDto ziraiDonemDto);

    ZiraiDonemDto entityToDto(ZiraiDonem ziraiDonem);

    List<ZiraiDonemDto> entityListToDtoList(List<ZiraiDonem> ziraiDonemList);

    List<ZiraiDonem> dtoListToEntityList(List<ZiraiDonemDto> ziraiDonemDtoList);
}
