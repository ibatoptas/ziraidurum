package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.dto.concrete.GroupDto;

public interface GroupService extends BaseService<GroupDto>{
}
