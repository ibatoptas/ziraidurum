package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.dto.concrete.DataInputDto;

public interface DataInputService extends BaseService<DataInputDto>{

}
