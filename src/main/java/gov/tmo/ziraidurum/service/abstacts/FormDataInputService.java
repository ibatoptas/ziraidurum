package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.FormDataInputInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.FormDataInputShortInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPAdId;
import gov.tmo.ziraidurum.dto.abstacts.projections.UrunInfo;
import gov.tmo.ziraidurum.dto.concrete.FormDataInputDto;
import gov.tmo.ziraidurum.entities.concrete.FormDataInput;

import java.util.List;

public interface FormDataInputService extends BaseService<FormDataInputDto>{

    DataResult<List<FormDataInputShortInfo>> getAllShort();

    DataResult<List<FormDataInputInfo>> getAll();

    DataResult<FormDataInputInfo> getById(Long id);

    DataResult<FormDataInputShortInfo> getByIdShort(Long id);
}
