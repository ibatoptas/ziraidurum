package gov.tmo.ziraidurum.service.abstacts;

import gov.tmo.ziraidurum.dto.concrete.ZiraiDonemSummaryDto;

import java.util.List;

/**
 * Created By ibrahim.toptas
 * Created At 14.10.2021 - 14:17
 **/
public interface QueryService {
    List<ZiraiDonemSummaryDto> getZiraiDonemSummary(Long ekimDonemId, Long urunId);
}
