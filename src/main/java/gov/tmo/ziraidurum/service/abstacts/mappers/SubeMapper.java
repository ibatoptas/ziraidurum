package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.SubeDto;
import gov.tmo.ziraidurum.dto.concrete.UrunDto;
import gov.tmo.ziraidurum.entities.concrete.Sube;
import gov.tmo.ziraidurum.entities.concrete.Urun;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SubeMapper {

    Sube dtoToEntity(SubeDto subeDto);

    SubeDto entityToDto(Sube sube);

    List<SubeDto> entityListToDtoList(List<Sube> subeList);

    List<Sube> dtoListToEntityList(List<SubeDto> subeDtoList);
}
