package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.OnayDto;
import gov.tmo.ziraidurum.dto.concrete.SubeDto;
import gov.tmo.ziraidurum.entities.concrete.Onay;
import gov.tmo.ziraidurum.entities.concrete.Sube;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OnayMapper {

    Onay dtoToEntity(OnayDto onayDto);

    OnayDto entityToDto(Onay onay);

    List<OnayDto> entityListToDtoList(List<Onay> onayList);

    List<Onay> dtoListToEntityList(List<OnayDto> onayDtoList);
}
