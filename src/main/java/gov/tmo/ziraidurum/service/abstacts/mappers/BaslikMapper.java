package gov.tmo.ziraidurum.service.abstacts.mappers;

import gov.tmo.ziraidurum.dto.concrete.BaslikDto;
import gov.tmo.ziraidurum.dto.concrete.BolgeDto;
import gov.tmo.ziraidurum.entities.concrete.Baslik;
import gov.tmo.ziraidurum.entities.concrete.Bolge;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BaslikMapper {

    Baslik dtoToEntity(BaslikDto baslikDto);

    BaslikDto entityToDto(Baslik baslik);

    List<BaslikDto> entityListToDtoList(List<Baslik> basliks);

    List<Baslik> dtoListToEntityList(List<BaslikDto> baslikDtoList);
}
