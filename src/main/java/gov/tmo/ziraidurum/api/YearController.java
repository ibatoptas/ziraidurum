package gov.tmo.ziraidurum.api;


import gov.tmo.ziraidurum.core.Utilities.Paths.ApiPaths;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.abstacts.projections.YearInfo;
import gov.tmo.ziraidurum.dto.concrete.YearDto;
import gov.tmo.ziraidurum.service.abstacts.YearService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = ApiPaths.YearController.CONTROLLER)
public class YearController {

    private final YearService yearService;

    public YearController(YearService yearService) {
        this.yearService = yearService;
    }

    @GetMapping("/getAll")
    @ApiOperation(value = "Get All Year Operation", response = String.class, responseContainer = "List")
    public DataResult<List<YearInfo>> getAll(){
        return this.yearService.getAll();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Get By Id Operation", response = YearInfo.class)
    public DataResult<YearInfo> getById(@PathVariable(value = "id",required = true)Long id){
        return this.yearService.getById(id);
    }

    @PostMapping
    @ApiOperation(value = "Create Operation", response = YearDto.class)
    public Result createYear(@Valid @RequestBody YearDto yearDto){
        return this.yearService.save(yearDto);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Operation", response = YearDto.class)
    public Result updateYear(@PathVariable(value = "id",required = true)Long id, @Valid @RequestBody YearDto yearDto)
    {
        return this.yearService.update(id,yearDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Operation",response = Boolean.class)
    public Result deleteYear(@PathVariable(value = "id",required = true)Long id){
        return this.yearService.delete(id);
    }

    @PutMapping("softDelete/{id}")
    @ApiOperation(value = "Soft Delete Operation",response = Boolean.class)
    public Result softDeleteYear(@PathVariable(value = "id",required = true)Long id){
        return this.yearService.softDelete(id);
    }
}
