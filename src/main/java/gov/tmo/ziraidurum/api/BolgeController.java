package gov.tmo.ziraidurum.api;

import gov.tmo.ziraidurum.core.Utilities.Paths.ApiPaths;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.abstacts.projections.BolgeInfo;
import gov.tmo.ziraidurum.dto.concrete.BolgeDto;
import gov.tmo.ziraidurum.service.abstacts.BolgeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = ApiPaths.BolgeController.CONTROLLER)
public class BolgeController {

    private final BolgeService bolgeService;

    public BolgeController(BolgeService bolgeService) {
        this.bolgeService = bolgeService;
    }

    @GetMapping("/getAll")
    @ApiOperation(value = "Get All Bolge Operation", response = String.class, responseContainer = "List")
    public DataResult<List<BolgeInfo>> getAll(){
        return this.bolgeService.getAll();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Get By Id Operation", response = BolgeInfo.class)
    public DataResult<BolgeInfo> getById(@PathVariable(value = "id",required = true)Long id){
        return this.bolgeService.getById(id);
    }

    @PostMapping
    @ApiOperation(value = "Create Operation", response = BolgeDto.class)
    public Result createBolge(@Valid @RequestBody BolgeDto bolgeDto){
        return this.bolgeService.save(bolgeDto);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Operation", response = BolgeDto.class)
    public Result updateBolge(@PathVariable(value = "id",required = true)Long id, @Valid @RequestBody BolgeDto bolgeDto)
    {
        return this.bolgeService.update(id,bolgeDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Operation",response = Boolean.class)
    public Result deleteBolge(@PathVariable(value = "id",required = true)Long id){
        return this.bolgeService.delete(id);
    }

    @PutMapping("softDelete/{id}")
    @ApiOperation(value = "Soft Delete Operation",response = Boolean.class)
    public Result softDeleteBolge(@PathVariable(value = "id",required = true)Long id){
        return this.bolgeService.softDelete(id);
    }
}
