package gov.tmo.ziraidurum.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import gov.tmo.ziraidurum.core.Utilities.Paths.ApiPaths;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.concrete.LoginDto;
import gov.tmo.ziraidurum.service.abstacts.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created By ibrahim.toptas
 * Created At 20.10.2021 - 11:07
 **/
@RestController
@RequiredArgsConstructor
@RequestMapping(value = ApiPaths.AuthController.CONTROLLER)
public class AuthController {
    private final AuthService authService;

    @PostMapping("login")
    public Result login(@RequestBody LoginDto loginDto) throws JsonProcessingException {
        return authService.login(loginDto);
    }
}
