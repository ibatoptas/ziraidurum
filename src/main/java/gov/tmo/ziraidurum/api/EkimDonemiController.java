package gov.tmo.ziraidurum.api;

import gov.tmo.ziraidurum.core.Utilities.Paths.ApiPaths;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.abstacts.projections.EkimDonemiInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPEkimDonemiShort;
import gov.tmo.ziraidurum.dto.concrete.EkimDonemiDto;
import gov.tmo.ziraidurum.service.abstacts.EkimDonemiService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = ApiPaths.EkimDonemiController.CONTROLLER)
public class EkimDonemiController {

    private final EkimDonemiService ekimDonemiService;

    public EkimDonemiController(EkimDonemiService ekimDonemiService) {
        this.ekimDonemiService = ekimDonemiService;
    }

    @GetMapping("/getAll")
    @ApiOperation(value = "Get All Ekim Dönemi Operation", response = String.class, responseContainer = "List")
    public DataResult<List<EkimDonemiInfo>> getAll(){
        return this.ekimDonemiService.getAll();
    }

    @GetMapping("getShort")
    @ApiOperation(value = "Get Short Ekim Dönemi Operation", response = String.class, responseContainer = "List")
    public DataResult<List<IPEkimDonemiShort>> getShortList(){
        return ekimDonemiService.getShortList();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Get By Id Operation", response = EkimDonemiInfo.class)
    public DataResult<EkimDonemiInfo> getById(@PathVariable(value = "id",required = true)Long id){
        return this.ekimDonemiService.getById(id);
    }

    @PostMapping
    @ApiOperation(value = "Create Operation", response = EkimDonemiDto.class)
    public Result createEkimDonemi(@Valid @RequestBody EkimDonemiDto ekimDonemiDto){
        return this.ekimDonemiService.save(ekimDonemiDto);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Operation", response = EkimDonemiDto.class)
    public Result updateEkimDonemi(@PathVariable(value = "id",required = true)Long id, @Valid @RequestBody EkimDonemiDto ekimDonemiDto)
    {
        return this.ekimDonemiService.update(id,ekimDonemiDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Operation",response = Boolean.class)
    public Result deleteEkimDonemi(@PathVariable(value = "id",required = true)Long id){
        return this.ekimDonemiService.delete(id);
    }

    @PutMapping("softDelete/{id}")
    @ApiOperation(value = "Soft Delete Operation",response = Boolean.class)
    public Result softDeleteEkimDonemi(@PathVariable(value = "id",required = true)Long id){
        return this.ekimDonemiService.softDelete(id);
    }

}
