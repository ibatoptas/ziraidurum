package gov.tmo.ziraidurum.api;

import gov.tmo.ziraidurum.core.Utilities.Paths.ApiPaths;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPAdId;
import gov.tmo.ziraidurum.dto.abstacts.projections.UrunInfo;
import gov.tmo.ziraidurum.dto.concrete.UrunDto;
import gov.tmo.ziraidurum.service.abstacts.UrunService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = ApiPaths.UrunController.CONTROLLER)
public class UrunController {

    private final UrunService urunService;

    public UrunController(UrunService urunService) {
        this.urunService = urunService;
    }

    @GetMapping("/getAll")
    @ApiOperation(value = "Get All Ürün Operation", response = String.class, responseContainer = "List")
    public DataResult<List<UrunInfo>> getAll(){
        return this.urunService.getAll();
    }

    @GetMapping("/getAllUrun")
    @ApiOperation(value = "Get All Ürün Operation", response = String.class, responseContainer = "List")
    public DataResult<List<IPAdId>> getAllUrun(){
        return this.urunService.getUrunList();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Get By Id Operation", response = UrunInfo.class)
    public DataResult<UrunInfo> getById(@PathVariable(value = "id",required = true)Long id){
        return this.urunService.getById(id);
    }

    @PostMapping
    @ApiOperation(value = "Create Operation", response = UrunDto.class)
    public Result createUrun(@Valid @RequestBody UrunDto urunDto){
        return this.urunService.save(urunDto);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Operation", response = UrunDto.class)
    public Result updateUrun(@PathVariable(value = "id",required = true)Long id, @Valid @RequestBody UrunDto urunDto)
    {
        return this.urunService.update(id,urunDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Operation",response = Boolean.class)
    public Result deleteUrun(@PathVariable(value = "id",required = true)Long id){
        return this.urunService.delete(id);
    }

    @PutMapping("softDelete/{id}")
    @ApiOperation(value = "Soft Delete Operation",response = Boolean.class)
    public Result softDeleteUrun(@PathVariable(value = "id",required = true)Long id){
        return this.urunService.softDelete(id);
    }

}
