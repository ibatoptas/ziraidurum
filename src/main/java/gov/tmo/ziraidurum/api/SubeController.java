package gov.tmo.ziraidurum.api;

import gov.tmo.ziraidurum.core.Utilities.Paths.ApiPaths;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPSubeShort;
import gov.tmo.ziraidurum.dto.abstacts.projections.SubeInfo;
import gov.tmo.ziraidurum.dto.concrete.SubeDto;
import gov.tmo.ziraidurum.service.abstacts.SubeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = ApiPaths.SubeController.CONTROLLER)
public class SubeController {

    private final SubeService subeService;

    public SubeController(SubeService subeService) {
        this.subeService = subeService;
    }

    @GetMapping("/getAll")
    @ApiOperation(value = "Get All Şube Operation", response = String.class, responseContainer = "List")
    public DataResult<List<SubeInfo>> getAll(){
        return this.subeService.getAll();
    }

    @GetMapping("getShortList")
    public DataResult<List<IPSubeShort>> getSubeShortList(){
        return subeService.getSubeShortList();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Get By Id Operation", response = SubeInfo.class)
    public DataResult<SubeInfo> getById(@PathVariable(value = "id",required = true)Long id){
        return this.subeService.getById(id);
    }

    @PostMapping
    @ApiOperation(value = "Create Operation", response = SubeDto.class)
    public Result createSube(@Valid @RequestBody SubeDto subeDto){
        return this.subeService.save(subeDto);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Operation", response = SubeDto.class)
    public Result updateSube(@PathVariable(value = "id",required = true)Long id, @Valid @RequestBody SubeDto subeDto)
    {
        return this.subeService.update(id,subeDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Operation",response = Boolean.class)
    public Result deleteSube(@PathVariable(value = "id",required = true)Long id){
        return this.subeService.delete(id);
    }

    @PutMapping("softDelete/{id}")
    @ApiOperation(value = "Soft Delete Operation",response = Boolean.class)
    public Result softDeleteSube(@PathVariable(value = "id",required = true)Long id){
        return this.subeService.softDelete(id);
    }
}
