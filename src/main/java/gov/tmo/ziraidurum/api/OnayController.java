package gov.tmo.ziraidurum.api;

import gov.tmo.ziraidurum.core.Utilities.Paths.ApiPaths;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.abstacts.projections.OnayInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.SubeInfo;
import gov.tmo.ziraidurum.dto.concrete.OnayDto;
import gov.tmo.ziraidurum.dto.concrete.SubeDto;
import gov.tmo.ziraidurum.service.abstacts.OnayService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = ApiPaths.OnayController.CONTROLLER)
public class OnayController {

    private final OnayService onayService;

    public OnayController(OnayService onayService) {
        this.onayService = onayService;
    }

    @GetMapping("/getAll")
    @ApiOperation(value = "Get All Onay Operation", response = String.class, responseContainer = "List")
    public DataResult<List<OnayInfo>> getAll(){
        return this.onayService.getAll();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Get By Id Operation", response = OnayInfo.class)
    public DataResult<OnayInfo> getById(@PathVariable(value = "id",required = true)Long id){
        return this.onayService.getById(id);
    }

    @PostMapping
    @ApiOperation(value = "Create Operation", response = OnayDto.class)
    public Result createOnay(@Valid @RequestBody OnayDto onayDto){
        return this.onayService.save(onayDto);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Operation", response = OnayDto.class)
    public Result updateOnay(@PathVariable(value = "id",required = true)Long id, @Valid @RequestBody OnayDto onayDto)
    {
        return this.onayService.update(id,onayDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Operation",response = Boolean.class)
    public Result deleteOnay(@PathVariable(value = "id",required = true)Long id){
        return this.onayService.delete(id);
    }

    @PutMapping("softDelete/{id}")
    @ApiOperation(value = "Soft Delete Operation",response = Boolean.class)
    public Result softDeleteOnay(@PathVariable(value = "id",required = true)Long id){
        return this.onayService.softDelete(id);
    }
}
