package gov.tmo.ziraidurum.api;

import gov.tmo.ziraidurum.core.Utilities.Paths.ApiPaths;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPZiraiDonemBase;
import gov.tmo.ziraidurum.dto.abstacts.projections.ZiraiDonemInfo;
import gov.tmo.ziraidurum.dto.concrete.ZiraiDonemDto;
import gov.tmo.ziraidurum.dto.concrete.ZiraiDonemSummaryDto;
import gov.tmo.ziraidurum.service.abstacts.ZiraiDonemService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = ApiPaths.ZiraiDonemController.CONTROLLER)
public class ZiraiDonemController {

    private final ZiraiDonemService ziraiDonemService;

    public ZiraiDonemController(ZiraiDonemService ziraiDonemService) {
        this.ziraiDonemService = ziraiDonemService;
    }

    @GetMapping("/getAll")
    @ApiOperation(value = "Get All Zirai Dönem Operation", response = String.class, responseContainer = "List")
    public DataResult<List<ZiraiDonemInfo>> getAll(){
        return this.ziraiDonemService.getAll();
    }

    @GetMapping("/getById/{id}")
    @ApiOperation(value = "Get By Id Operation", response = ZiraiDonemInfo.class)
    public DataResult<ZiraiDonemInfo> getById(@PathVariable(value = "id",required = true)Long id){
        return this.ziraiDonemService.getById(id);
    }

   @GetMapping("getSummary/{ekimDonemId}/{urunId}")
   public DataResult<List<ZiraiDonemSummaryDto>> getSummary(@PathVariable Long ekimDonemId,@PathVariable Long urunId){
       return this.ziraiDonemService.getSummary(ekimDonemId, urunId);
   }
   @GetMapping("getShortList/{ekimDonemId}/{urunId}")
   public DataResult<List<IPZiraiDonemBase>> getZiraiDonemsByDonemAndUrun(@PathVariable Long ekimDonemId, @PathVariable Long urunId){
       return ziraiDonemService.getZiraiDonemsByDonemAndUrunForTasra(ekimDonemId, urunId);
   }


    @PostMapping
    @ApiOperation(value = "Create Operation", response = ZiraiDonemDto.class)
    public Result createZiraiDonem(@Valid @RequestBody ZiraiDonemDto ziraiDonemDto){
        return this.ziraiDonemService.save(ziraiDonemDto);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Operation", response = ZiraiDonemDto.class)
    public Result updateZiraiDonem(@PathVariable(value = "id",required = true)Long id, @Valid @RequestBody ZiraiDonemDto ziraiDonemDto)
    {
        return this.ziraiDonemService.update(id,ziraiDonemDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Operation",response = Boolean.class)
    public Result deleteZiraiDonem(@PathVariable(value = "id",required = true)Long id){
        return this.ziraiDonemService.delete(id);
    }

    @PutMapping("softDelete/{id}")
    @ApiOperation(value = "Soft Delete Operation",response = Boolean.class)
    public Result softDeleteZiraiDonem(@PathVariable(value = "id",required = true)Long id){
        return this.ziraiDonemService.softDelete(id);
    }
}
