package gov.tmo.ziraidurum.api;

import gov.tmo.ziraidurum.core.Utilities.Paths.ApiPaths;
import gov.tmo.ziraidurum.core.Utilities.Results.Result;
import gov.tmo.ziraidurum.dto.concrete.TasraFormSaveDto;
import gov.tmo.ziraidurum.dto.concrete.UrunBaslikSaveDto;
import gov.tmo.ziraidurum.service.abstacts.FormModelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(value = ApiPaths.FormModelController.CONTROLLER)
public class FormModelController {

    private final FormModelService formModelService;

    public FormModelController(FormModelService formModelService) {
        this.formModelService = formModelService;
    }

    @PostMapping
    @ApiOperation(value = "Create Operation", response = UrunBaslikSaveDto.class)
    public Result createForm(@Valid @RequestBody UrunBaslikSaveDto urunBaslikSaveDto){
        return this.formModelService.saveFormModel(urunBaslikSaveDto);
    }

    @GetMapping("formDataInput/{ziraiDonemId}")
    public  Result getFromDataInputs(@PathVariable Long ziraiDonemId){
        return formModelService.getFormDataInputs(ziraiDonemId);
    }

    @GetMapping("formDataInput/{ziraiDonemId}/sube/{subeId}")
    public Result getFormDataInputsBySube(@PathVariable Long ziraiDonemId, @PathVariable Long subeId) {
        return formModelService.getFormDataInputsBySube(ziraiDonemId, subeId);
    }

    @GetMapping("formDataInput/{donemId}/{urunId}/sube/{subeId}")
    public Result getFormDataInputsByUrunAndSube(@PathVariable Long donemId, @PathVariable Long urunId, @PathVariable Long subeId) {
        return formModelService.getFormDataInputsByUrunAndSube(donemId, urunId, subeId);
    }

    @PostMapping("formDataInput/{act}/{formId}")
    public Result setFormDataInputOnay(@PathVariable Boolean act, @PathVariable Long formId) {
        return formModelService.setFormDataInputOnay(act, formId);
    }

    @PostMapping("dataInputs")
    public Result saveFromDatas(@RequestBody @Valid TasraFormSaveDto dto) {
        return formModelService.saveFormDatas(dto);
    }

    @PostMapping("onay/{act}/{formDataInputId}")
    public Result updateFormDataInputOnay(@PathVariable Boolean act, @PathVariable Long formDataInputId) {
        return formModelService.updateFormDataInputOnay(act, formDataInputId);
    }

}
