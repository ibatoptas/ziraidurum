package gov.tmo.ziraidurum.api;


import gov.tmo.ziraidurum.core.Utilities.Paths.ApiPaths;
import gov.tmo.ziraidurum.core.Utilities.Results.DataResult;
import gov.tmo.ziraidurum.dto.abstacts.projections.BaslikInfo;
import gov.tmo.ziraidurum.dto.abstacts.projections.IPBaslik;
import gov.tmo.ziraidurum.dto.abstacts.projections.UrunInfo;
import gov.tmo.ziraidurum.service.abstacts.BaslikService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = ApiPaths.BaslikController.CONTROLLER)
public class BaslikController {

    private final BaslikService baslikService;

    public BaslikController(BaslikService baslikService) {
        this.baslikService = baslikService;
    }

    @GetMapping("/getAll")
    @ApiOperation(value = "Get All Baslik Operation", response = String.class, responseContainer = "List")
    public DataResult<List<IPBaslik>> getAll(){
        return this.baslikService.getAll();
    }

    @GetMapping("/getBasliksByUrun/{id}")
    @ApiOperation(value = "Get All Baslik By Urun Operation", response = String.class, responseContainer = "List")
    public DataResult<List<IPBaslik>> getBasliksByUrun(@PathVariable(value = "id",required = true)Long id){
        return this.baslikService.getBasliksByUrunId(id);
    }
}
