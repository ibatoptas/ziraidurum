package gov.tmo.ziraidurum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZiraidurumApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZiraidurumApplication.class, args);
    }

}
