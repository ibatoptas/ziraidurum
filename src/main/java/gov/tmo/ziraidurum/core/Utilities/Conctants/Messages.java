package gov.tmo.ziraidurum.core.Utilities.Conctants;

public class Messages {

    public static final String urunListed ="Ürünler listelendi !";
    public static final String urunGetted ="Id'ye göre Ürün getirildi !";
    public static final String urunSaved ="Ürün kaydedildi !";
    public static final String urunUpdated ="Ürün güncellendi !";
    public static final String urunDeleted ="Ürün silindi !";
    public static final String urunCodeError ="Ürün kodu daha önce tanımlanmış !";

    public static final String yearListed ="Yıllar listelendi !";
    public static final String yearGetted ="Id'ye göre Yıl getirildi !";
    public static final String yearSaved ="Yıl kaydedildi !";
    public static final String yearUpdated ="Yıl güncellendi !";
    public static final String yearDeleted ="Yıl silindi !";
    public static final String yearNameError ="Girilen yıl daha önce tanımlanmış !";

    public static final String bolgeListed ="Bölgeler listelendi !";
    public static final String bolgeGetted ="Id'ye göre Bölge getirildi !";
    public static final String bolgeSaved ="Bölge kaydedildi !";
    public static final String bolgeUpdated ="Bölge güncellendi !";
    public static final String bolgeDeleted ="Bölge silindi !";

    public static final String subeListed ="Şubeler listelendi !";
    public static final String subeGetted ="Id'ye göre Şube getirildi !";
    public static final String subeSaved ="Şube kaydedildi !";
    public static final String subeUpdated ="Şube güncellendi !";
    public static final String subeDeleted ="Şube silindi !";

    public static final String ekimDonemiListed ="Ekim Dönemleri listelendi !";
    public static final String ekimDonemiGetted ="Id'ye göre Ekim dönemi getirildi !";
    public static final String ekimDonemiSaved ="Ekim dönemi kaydedildi !";
    public static final String ekimDonemiSavedError ="Ekim dönemi kaydedilirken bir hata meydana geldi !";
    public static final String ekimDonemiUpdated ="Ekim dönemi güncellendi !";
    public static final String ekimDonemiDeleted ="Ekim dönemi silindi !";

    public static final String ziraiDonemListed ="Zirai Dönemler listelendi !";
    public static final String ziraiDonemGetted ="Id'ye göre Zirai dönemi getirildi !";
    public static final String ziraiDonemSaved ="Zirai dönem kaydedildi !";
    public static final String ziraiDonemUpdated ="Zirai dönem güncellendi !";
    public static final String ziraiDonemDeleted ="Zirai dönem silindi !";

    public static final String seviyeOneListed ="Seviye 1 ler listelendi !";
    public static final String seviyeOneGetted ="Id'ye göre Seviye 1 getirildi !";
    public static final String seviyeOneSaved ="Seviye 1 kaydedildi !";
    public static final String seviyeOneUpdated ="Seviye 1 güncellendi !";
    public static final String seviyeOneDeleted ="Seviye 1 silindi !";

    public static final String seviyeTwoListed ="Seviye 2 ler listelendi !";
    public static final String seviyeTwoGetted ="Id'ye göre Seviye 2 getirildi !";
    public static final String seviyeTwoSaved ="Seviye 2 kaydedildi !";
    public static final String seviyeTwoUpdated ="Seviye 2 güncellendi !";
    public static final String seviyeTwoDeleted ="Seviye 2 silindi !";

    public static final String seviyeThreeListed ="Seviye 3 ler listelendi !";
    public static final String seviyeThreeGetted ="Id'ye göre Seviye 3 getirildi !";
    public static final String seviyeThreeSaved ="Seviye 3 kaydedildi !";
    public static final String seviyeThreeUpdated ="Seviye 3 güncellendi !";
    public static final String seviyeThreeDeleted ="Seviye 3 silindi !";

    public static final String seviyeDataListed ="Seviye Data lar listelendi !";
    public static final String seviyeDataGetted ="Id'ye göre Seviye Data getirildi !";
    public static final String seviyeDataSaved ="Seviye Data kaydedildi !";
    public static final String seviyeDataUpdated ="Seviye Data güncellendi !";
    public static final String seviyeDataDeleted ="Seviye Data silindi !";

    public static final String ekimUrunDonemDataListed ="Ekim Ürün Dönemler listelendi !";
    public static final String ekimUrunDonemGetted ="Id'ye göre Ekim Ürün Dönem getirildi !";
    public static final String ekimUrunDonemSaved ="Ekim Ürün Dönem kaydedildi !";
    public static final String ekimUrunDonemUpdated ="Ekim Ürün Dönem güncellendi !";
    public static final String ekimUrunDonemDeleted ="Ekim Ürün Dönem silindi !";

    public static final String formDataListed ="Formlar listelendi !";
    public static final String formGetted ="Id'ye göre Ekim Form getirildi !";
    public static final String formSaved ="Form kaydedildi !";
    public static final String formUpdated ="Form güncellendi !";
    public static final String formDeleted ="Form silindi !";

    public static final String onayDataListed ="Onaylar listelendi !";
    public static final String onayGetted ="Id'ye göre Onay getirildi !";
    public static final String onaySaved ="Onay kaydedildi !";
    public static final String onayUpdated ="Onay güncellendi !";
    public static final String onayDeleted ="Onay silindi !";

    public static final String formModelSaved ="Form Model kaydedildi !";

    public static final String baslikListed ="Başlıklar listelendi !";
    public static final String baslikListedByUrun ="Başlıklar ürünlere göre listelendi !";
}
