package gov.tmo.ziraidurum.core.Utilities.Paths;

public class ApiPaths {

    private static final String BASE_PATH = "/api";

    public static final class UrunController{
        public static final String CONTROLLER = BASE_PATH+"/urun";
    }

    public static final class BolgeController{
        public static final String CONTROLLER = BASE_PATH+"/bolge";
    }

    public static final class SubeController{
        public static final String CONTROLLER = BASE_PATH+"/sube";
    }

    public static final class OnayController{
        public static final String CONTROLLER = BASE_PATH+"/onay";
    }

    public static final class EkimDonemiController{
        public static final String CONTROLLER = BASE_PATH+"/ekimdonemi";
    }

    public static final class ZiraiDonemController{
        public static final String CONTROLLER = BASE_PATH+"/ziraidonem";
    }


    public static final class EkimUrunDonemController{
        public static final String CONTROLLER = BASE_PATH+"/ekimurundonem";
    }

    public static final class YearController{
        public static final String CONTROLLER = BASE_PATH + "/year";
    }

    public static final class FormModelController {
        public static final String CONTROLLER = BASE_PATH + "/formmodel";
    }

    public static final class BaslikController {
        public static final String CONTROLLER = BASE_PATH + "/baslik";
    }

    public static final class AuthController {
        public static final String CONTROLLER = BASE_PATH + "/auth";
    }
}
