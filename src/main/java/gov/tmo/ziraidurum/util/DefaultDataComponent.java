package gov.tmo.ziraidurum.util;

import gov.tmo.ziraidurum.entities.concrete.*;
import gov.tmo.ziraidurum.repository.concrete.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by iBa at 14.10.2021 22:31
 */
@Component
@RequiredArgsConstructor
public class DefaultDataComponent {
    private final YearRepository yearRepository;
    private final UrunRepository urunRepository;
    private final EkimDonemiRepository ekimDonemiRepository;
    private final UserRepository userRepository;
    private final SubeRepository subeRepository;
    private final BolgeRepository bolgeRepository;

    @PostConstruct
    public void defData(){
        if (yearRepository.count()==0){
            Year y = new Year();
            y.setYearName("2021");
            y.setId(1l);
            y.setStatus(true);
            yearRepository.save(y);
        }
        if (urunRepository.count()==0){
            Urun urun = new Urun();
            urun.setUrunAdi("Çeltik");
            urun.setUrunKodu("123");
            urun.setStatus(true);
            urunRepository.save(urun);
        }
        if (ekimDonemiRepository.count()==0){
            EkimDonemi ed = new EkimDonemi();
            ed.setEkimAdi("2021");
            ed.setYear(Util.createEntityWithId(1l,Year.class));
            ed.setStatus(true);
            ekimDonemiRepository.save(ed);
        }
        if (bolgeRepository.count()==0){
            Bolge b = new Bolge();
            b.setId(1l);
            b.setName("bölge");
            b.setStatus(true);
            bolgeRepository.save(b);
        }
        if (subeRepository.count()==0){
            Sube s = new Sube();
            s.setBolge(Util.createEntityWithId(1l,Bolge.class));
            s.setStatus(true);
            s.setSubeAdi("Şube Adı");
            s.setSubeKodu("ŞubeKod");
            s.setId(1l);
            subeRepository.save(s);
        }
        if (userRepository.count()==0){
            User u = new User();
            u.setStatus(true);
            u.setEmail("email");
            u.setId(1l);
            u.setHasOnay(true);
            u.setKimlikname("name");
            u.setKimliksurname("surname");
            u.setPosition("a");
            u.setUsername("username");
            u.setSubeUser(Util.createEntityWithId(1l,Sube.class));
            u.setId(1l);
            userRepository.save(u);
        }
    }
}
