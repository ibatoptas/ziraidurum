package gov.tmo.ziraidurum.util;

import gov.tmo.ziraidurum.entities.concrete.BaseEntity;

import java.util.ArrayList;
import java.util.List;

public class Util {

    public static <T extends BaseEntity> T createEntityWithId(Long id, Class<T> clazz) {
        try {
            T t = clazz.getDeclaredConstructor().newInstance();
            t.setId(id);
            return t;
        } catch (Exception e) {
            throw new IllegalArgumentException((e.getMessage()));
        }
    }
    public static <T extends BaseEntity> List<T> createEntityWidthId(List<Long> ids, Class<T> clz){
        List<T> result = new ArrayList<>(ids.size());
        for (Long id : ids) {
            result.add(createEntityWithId(id, clz));
        }
        return result;
    }

}
