package gov.tmo.ziraidurum.util;

import gov.tmo.ziraidurum.entities.concrete.FormDataInput;
import gov.tmo.ziraidurum.entities.concrete.enums.ERoleType;
import gov.tmo.ziraidurum.entities.concrete.enums.EFormOnayStatus;
import gov.tmo.ziraidurum.service.abstacts.SharedService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Created By ibrahim.toptas
 * Created At 18.10.2021 - 09:35
 **/
@Component("utilBeans")
@RequiredArgsConstructor
public class UtilityBeans {
    private final SharedService sharedService;

    public boolean isFormDataInputReadonly(FormDataInput formDataInput){
        ERoleType userRole= sharedService.getUserRole();
        return  !(userRole == ERoleType.TASRA && formDataInput.getFormOnayStatus() != EFormOnayStatus.MERKEZ);
    }
}
